@extends('adminlte::page')

@section('content')
@include('include.breadcrumbs', ['breadcrumbs' => [
    'Admin' => '#',
    'Feedback' => 'all-feedback',
    'Add Feedback' => route('add-feedback'),

]])
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Add Feedback') }}</div>

                <div class="card-body">
                    <form method="PUT" action="{{ route('submit_feedback') }}">
                        @csrf

                        <div class="form-group row">
                            <label for="feedback" class="col-md-4 col-form-label text-md-right">{{ __('Feedback') }}</label><span style="color:red"> *</span>

                            <div class="col-md-6">
                                <input id="feedback" type="text" class="form-control @error('feedback') is-invalid @enderror" name="feedback" value="{{ old('feedback') }}" required autocomplete="feedback">

                                @error('feedback')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                        
                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Submit') }}
                                </button>
                                <input type="button" onclick="history.go(-1);" value="Back" class="btn btn-primary">
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@include('footerimport')
@endsection
