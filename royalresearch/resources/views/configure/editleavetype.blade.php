@extends('adminlte::page')

@section('content')
@include('include.breadcrumbs', ['breadcrumbs' => [
    'Leave' => '#',
    'Configure' => '#',
    'Leave Type' => route('view-leave-type'),
    'Edit Leave Type' => '#',

]])
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('EDIT Leave Type') }}</div>

                <div class="card-body">
                    <form method="PUT" action="{{ route('update-leave-type', $leavetype->id) }}">
                        @csrf

                        <div class="form-group row">
                            <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Name') }}</label><span style="color:red"> *</span>

                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ $leavetype->name }}" required autocomplete="name" autofocus>

                                @error('name')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="exclude_in_reports_if_no_entitlement" class="col-md-4 col-form-label text-md-right">{{ __('Is entitlement situational') }}</label>

                            <div class="col-md-6">
                                <input id="exclude_in_reports_if_no_entitlement" type="checkbox" class="form-control" name="exclude_in_reports_if_no_entitlement" value="{{ old('exclude_in_reports_if_no_entitlement') }}">

                            </div>
                        </div>
                        
                       
                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Save') }}
                                </button>
                                <input type="button" onclick="history.go(-1);" value="Back" class="btn btn-primary">
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@include('footerimport')
@endsection
