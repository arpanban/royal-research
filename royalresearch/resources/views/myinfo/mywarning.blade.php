@extends('adminlte::page')

@section('content')
@include('include.breadcrumbs', ['breadcrumbs' => [
    'Warning' => '#',
    

]])
@section('plugins.Datatables', true)
    
    <div class="panel panel-default">
        <div class="panel-body">
        <div class="row">
    <div class="form-group col-md-6">
                <h2>Employee Warning</h2>
            </div>
            <div class="form-group col-md-6"; align="right">
                <a class="btn btn-success" href="{{ route('add-employeeexamscore') }}"><i class="fas fa-plus-square"></i></a>
            </div>
            
        </div>
            <div class="table-responsive">
           
        
               
                <table id="myTable" class="table table-bordered table-striped {{ count($warning) > 0 ? 'datatable' : '' }} pointer">
                    <thead>
                    <tr>
                        
                        
                        <th>Employee Name</th>
                        <th>Warning Given Name</th>
                        <th>Warning Header</th>  
                        <th>Reason</th>
                        <th>Date</th>
     
                    </tr>
                    </thead>

                    <tbody>
                    @if (count($warning) > 0)
                        @foreach ($warning as $reports)
                            <tr data-entry-id="{{ $reports->id }}">
                                
                                
                                <td>{{ $reports->warning_emp_name }}</td>
                                <td>{{ $reports->issuer_name }}</td>
                                <td>{{ $reports->warning_header }}</td>
                                <td>{{ $reports->reason }}</td>
                                <td>{!! \Carbon\Carbon::parse($reports->date)->format('d M Y')  !!}</td>
                         
                            </tr>
                        @endforeach
                    @else
                        <tr>
                            <td colspan="7">No entries in table</td>
                        </tr>
                    @endif
                    </tbody>
                </table>

            </div>
        </div>
    </div>
    @include('footerimport')
    @include('datatable')
    @endsection
  
