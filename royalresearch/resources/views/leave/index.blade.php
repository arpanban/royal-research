@extends('adminlte::page')

@section('content')
@include('include.breadcrumbs', ['breadcrumbs' => [
    'Leave' => route('leave'),

]])

    <div id="main-wrapper">
    
        <div class="page-wrapper">
            <div class="page-breadcrumb">
                <div class="row">
                    <div class="col-12 d-flex no-block align-items-center">
                        <h4 class="page-title">Leave Management</h4>
                        <div class="ml-auto text-right">
                            
                        </div>
                    </div>
                </div>
            </div>

           
                <div class="row">
                    <div class="col-md-2">
                        @can('isEmployee')
                        <a class="btn btn-lg btn-dark" href="{{route('leave.create')}}">Apply leave</a>
                        @endcan
                    </div>
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                               
                                <div class="table-responsive">
                                    <table id="zero_config" class="table table-striped table-bordered">
                                        <thead>
                                        <tr>
                                            <th>S.N.</th>
                                            <th>Employee Name</th>
                                            <th>Leave type</th>
                                            <th>Date from</th>
                                            <th>Date to</th>
                                            <th>No. of days</th>
                                            <th>Reason</th>
                                            <th> Admin Approval</th>
                                            <th>Action</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($leaves as $leave)
                                            <tr>
                                                <td>{{$loop -> index+1 }}</td>
                                                <td>{{$leave->emp_nick_name}}</td>
                                                <td>{{$leave->name}}</td>
                                                <td>{{$leave->date_from}}</td>
                                                <td>{{$leave->date_to}}</td>
                                                <td>{{$leave->days}}</td>
                                                <td>{{$leave->reason}}</td>
                                                <td>
                                                    @if(Auth::user()->role=='1' || Auth::user()->role=='2')
                                                        {{--{{$leave->is_approved}}--}}
                                                        @if($leave->leave_type_offer==0)
                                                            <form id="{{$leave->id}}" action="{{route('leave.paid',$leave->id)}}" method="POST">
                                                                @csrf
                                                                {{--<button type="button" onclick="approveLeave({{$leave->id}})" class="btn btn-sm btn-success" name="approve" value="1">Approve</button>--}}
                                                                <button type="submit" onclick="return confirm('Are you sure want to approve for leave?')" class="btn btn-sm btn-success" name="paid" value="1">Approve</button>
                                                            </form>
                                                            <form id="{{$leave->id}}" action="{{route('leave.paid',$leave->id)}}" method="POST">
                                                                @csrf
                                                                {{--<button type="button" onclick="rejectLeave({{$leave->id}})" class="btn btn-sm btn-danger" name="approve" value="2">Reject</button>--}}
                                                                <button type="submit" onclick="return confirm('Are you sure want to reject for leave?')" class="btn btn-sm btn-danger" name="paid" value="2">Reject</button>
                                                            </form>
                                                        @elseif($leave->leave_type_offer==1)

                                                            <form action="{{route('leave.paid',$leave->id)}}" method="POST">
                                                                @csrf
                                                                <button class="btn btn-sm btn-danger" onclick="return confirm('Are you sure want to reject for leave?')" type="submit" name="paid" value="2">Reject</button>
                                                            </form>
                                                        @else
                                                            <form action="{{route('leave.paid',$leave->id)}}" method="POST">
                                                                @csrf
                                                                <button class="btn btn-sm btn-success" onclick="return confirm('Are you sure want to approve for leave?')" type="submit" name="paid" value="1">Approve</button>
                                                            </form>
                                                        @endif

                                                        {{--<a href="{{route('leave.approve',$leave->id)}}" class="btn btn-sm btn-success">Approve</a>--}}
                                                        {{--<a href="{{route('leave.pending',$leave->id)}}" class="btn btn-sm btn-warning">Pending</a>--}}
                                                        {{--<a href="{{route('leave.reject',$leave->id)}}" class="btn btn-sm btn-danger">Reject</a>--}}
                                                    @else
                                                        @if($leave->leave_type_offer==0)
                                                            <span class="badge badge-pill badge-warning">Pending</span>
                                                        @elseif($leave->leave_type_offer==1)
                                                            <span class="badge badge-pill badge-success">Approve</span>
                                                        @else
                                                            <span class="badge badge-pill badge-danger">Reject</span>
                                                        @endif
                                                    @endif
                                                </td>

                                                        <td>
                                                            @if(Auth::user()->role=='3'|| Auth::user()->role=='4')
                                                            {{--{{$leave->is_approved}}--}}
                                                            @if($leave->is_approved==0)
                                                                <form id="approve-leave-{{$leave->id}}" action="{{route('leave.approve',$leave->id)}}" method="POST">
                                                                    @csrf
                                                                    {{--<button type="button" onclick="approveLeave({{$leave->id}})" class="btn btn-sm btn-success" name="approve" value="1">Approve</button>--}}
                                                                    <button type="submit" onclick="return confirm('Are you sure want to approve leave?')" class="btn btn-sm btn-success" name="approve" value="1">Approve</button>
                                                                </form>
                                                                <form id="reject-leave-{{$leave->id}}" action="{{route('leave.approve',$leave->id)}}" method="POST">
                                                                    @csrf
                                                                    {{--<button type="button" onclick="rejectLeave({{$leave->id}})" class="btn btn-sm btn-danger" name="approve" value="2">Reject</button>--}}
                                                                    <button type="submit" onclick="return confirm('Are you sure want to reject leave?')" class="btn btn-sm btn-danger" name="approve" value="2">Reject</button>
                                                                </form>
                                                            @elseif($leave->is_approved==1)

                                                                <form action="{{route('leave.approve',$leave->id)}}" method="POST">
                                                                    @csrf
                                                                    <button class="btn btn-sm btn-danger" onclick="return confirm('Are you sure want to reject leave?')" type="submit" name="approve" value="2">Reject</button>
                                                                </form>
                                                            @else
                                                                <form action="{{route('leave.approve',$leave->id)}}" method="POST">
                                                                    @csrf
                                                                    <button class="btn btn-sm btn-success" onclick="return confirm('Are you sure want to approve leave?')" type="submit" name="approve" value="1">Approve</button>
                                                                </form>
                                                            @endif

                                                                {{--<a href="{{route('leave.approve',$leave->id)}}" class="btn btn-sm btn-success">Approve</a>--}}
                                                                {{--<a href="{{route('leave.pending',$leave->id)}}" class="btn btn-sm btn-warning">Pending</a>--}}
                                                                {{--<a href="{{route('leave.reject',$leave->id)}}" class="btn btn-sm btn-danger">Reject</a>--}}
                                                                @else
                                                                @if($leave->is_approved==0)
                                                                    <span class="badge badge-pill badge-warning">Pending</span>
                                                                @elseif($leave->is_approved==1)
                                                                    <span class="badge badge-pill badge-success">Approved</span>
                                                                @else
                                                                    <span class="badge badge-pill badge-danger">Rejected</span>
                                                                @endif
                                                            @endif
                                                </td>
                                            </tr>
                                        </tbody>
                                        @endforeach
                                    </table>
                                    {{ $leaves->links() }}
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
            @include('footerimport')
            {{--sweetalert box for deleting start--}}
            {{--<script src="https://cdn.jsdelivr.net/npm/sweetalert2@7.28.8/dist/sweetalert2.all.min.js"></script>--}}

            {{--<script type="text/javascript">--}}
                {{--function rejectLeave(id)--}}

                {{--{--}}
                    {{--const swalWithBootstrapButtons = swal.mixin({--}}
                        {{--confirmButtonClass: 'btn btn-success',--}}
                        {{--cancelButtonClass: 'btn btn-danger',--}}
                        {{--buttonsStyling: false,--}}
                    {{--})--}}

                    {{--swalWithBootstrapButtons({--}}
                        {{--title: 'Are you sure?',--}}
                        {{--text: "You won't be able to do again this!",--}}
                        {{--type: 'warning',--}}
                        {{--showCancelButton: true,--}}
                        {{--confirmButtonText: 'Yes, reject it!',--}}
                        {{--cancelButtonText: 'No, cancel!',--}}
                        {{--reverseButtons: true--}}
                    {{--}).then((result) => {--}}
                        {{--if (result.value) {--}}
                            {{--event.preventDefault();--}}
                            {{--document.getElementById('reject-leave-'+id).submit();--}}
                        {{--} else if (--}}
                            {{--// Read more about handling dismissals--}}
                            {{--result.dismiss === swal.DismissReason.cancel--}}
                        {{--) {--}}
                            {{--swalWithBootstrapButtons(--}}
                                {{--'Cancelled',--}}
                                {{--'You have not cancel yet ! Your are safe :)',--}}
                                {{--'error'--}}
                            {{--)--}}
                        {{--}--}}
                    {{--})--}}
                {{--}--}}

            {{--</script>--}}
            {{--<script type="text/javascript">--}}
                {{--function approveLeave(id)--}}

                {{--{--}}
                    {{--const swalWithBootstrapButtons = swal.mixin({--}}
                        {{--confirmButtonClass: 'btn btn-success',--}}
                        {{--cancelButtonClass: 'btn btn-danger',--}}
                        {{--buttonsStyling: false,--}}
                    {{--})--}}

                    {{--swalWithBootstrapButtons({--}}
                        {{--title: 'Are you sure?',--}}
                        {{--text: "You want to approve leave!",--}}
                        {{--type: 'warning',--}}
                        {{--showCancelButton: true,--}}
                        {{--confirmButtonText: 'Yes, approve leave!',--}}
                        {{--cancelButtonText: 'No, cancel!',--}}
                        {{--reverseButtons: true--}}
                    {{--}).then((result) => {--}}
                        {{--if (result.value) {--}}
                            {{--event.preventDefault();--}}
                            {{--document.getElementById('approve-leave-'+id).submit();--}}
                        {{--} else if (--}}
                            {{--// Read more about handling dismissals--}}
                            {{--result.dismiss === swal.DismissReason.cancel--}}
                        {{--) {--}}
                            {{--swalWithBootstrapButtons(--}}
                                {{--'Cancelled',--}}
                                {{--'You are safe.You can approve later :)',--}}
                                {{--'error'--}}
                            {{--)--}}
                        {{--}--}}
                    {{--})--}}
                {{--}--}}

            {{--</script>--}}
            {{--sweetalert box for deleting end--}}
            
        </div>
    </div>

@endsection