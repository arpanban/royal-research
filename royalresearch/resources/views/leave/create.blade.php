@extends('adminlte::page')

@section('content')
@include('include.breadcrumbs', ['breadcrumbs' => [
    'Leave' => route('leave'),
    
    'Apply Leave' => route('leave.create'),

]])

    <div class="page-wrapper">
        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
    @endif

            

    <div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Apply Leave') }}</div>

                        
                            <div class="card-body">
                            <form action="{{route('leave.store')}}" method="post" class="form-horizontal">
                            @csrf
                            <div class="form-group row">
                            <label for="leave_type" class="col-md-4 col-form-label text-md-right">{{ __('Leave Type') }}</label><span style="color:red"> *</span>

                            <div class="col-sm-6">
                               
                            <select  name="leave_type" id="leave_type" class="form-control @error('leave_type') is-invalid @enderror"  value="{{ old('leave_type') }}" required autocomplete="leave_type">
                                 <option>--Select Leave Type--</option>
                                 @foreach($leavetype as $leavetypes)
                                        <option value="{{$leavetypes->id}}">{{$leavetypes->name}}</option>
                                    @endforeach
                                                                            
                                                     
                             </select>
                                @error('leave_type')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                                
                                <div class="form-group row">
                                    <label for="lname" class="col-sm-3 text-right control-label col-form-label">Date from</label><span style="color:red"> *</span>
                                    <div class="col-sm-4">
                                        <input type="date" min="{{date('Y-m-d')}}" name="date_from" class="form-control" id="FromDate">
                                    </div>
                                    <div class="col-sm-4">
                                        <input type="date" name="date_to" class="form-control" id="ToDate">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="fname" class="col-sm-3 text-right control-label col-form-label">Days</label><span style="color:red"> *</span>
                                    <div class="col-sm-6">
                                        <input type="text" name="days" class="form-control" id="TotalDays" placeholder="Number of leave days">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="fname" class="col-sm-3 text-right control-label col-form-label">Reason</label><span style="color:red"> *</span>
                                    <div class="col-sm-6">
                                        <textarea type="text" name="reason" class="form-control" placeholder="Reason"></textarea>
                                        </div>
                                </div>
                            
                           
                            <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Apply') }}
                                </button>
                                <input type="button" onclick="history.go(-1);" value="Back" class="btn btn-primary">
                            </div>
                        </div>
                            
                        </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
       

    </div>
    @include('footerimport')
@endsection

@section('js')
    <script>
        $("#ToDate").change(function () {
            var start = new Date($('#FromDate').val());
            var end = new Date($('#ToDate').val());

            var diff = new Date(end - start);
            var days=1;
            days = diff / 1000 / 60 / 60 / 24;

            // $('#TotalDays').val(days);
            if (days == NaN) {
                $('#TotalDays').val(0);
            } else {
                $('#TotalDays').val(days+1);
            }
        })

        $("#FromDate").change(function () {
            var start = new Date($('#FromDate').val());
            var end = new Date($('#ToDate').val());

            var diff = new Date(end - start);
            var days=1;
            days = diff / 1000 / 60 / 60 / 24;

            // $('#TotalDays').val(days);
            if (days == NaN) {
                $('#TotalDays').val(0);
            } else {
                $('#TotalDays').val(days+1);
            }
        })
    </script>
    @endsection