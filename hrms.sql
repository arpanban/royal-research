-- phpMyAdmin SQL Dump
-- version 5.0.4
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Apr 07, 2021 at 04:19 AM
-- Server version: 8.0.22
-- PHP Version: 7.3.25

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `hrms`
--

-- --------------------------------------------------------

--
-- Table structure for table `announcements`
--

CREATE TABLE `announcements` (
  `id` bigint UNSIGNED NOT NULL,
  `company_id` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  `location_id` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  `designation_id` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  `emp_id` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  `custom` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `text` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `active` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `azhrms_banks`
--

CREATE TABLE `azhrms_banks` (
  `id` int NOT NULL,
  `bank_name` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `bank_ifsc_no` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `branch_name` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `azhrms_banks`
--

INSERT INTO `azhrms_banks` (`id`, `bank_name`, `bank_ifsc_no`, `branch_name`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'State Bank Of India', 'sbin111002', 'Behala', '2021-03-29 02:39:01', '2021-03-29 02:39:01', NULL),
(2, 'Punjab National Bank', 'pun110256', 'Saltlake', '2021-03-29 02:39:23', '2021-03-29 02:39:39', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `azhrms_company_assets`
--

CREATE TABLE `azhrms_company_assets` (
  `id` int NOT NULL,
  `assets_name` varchar(200) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `assets_details` varchar(200) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL ON UPDATE CURRENT_TIMESTAMP,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `azhrms_company_assets`
--

INSERT INTO `azhrms_company_assets` (`id`, `assets_name`, `assets_details`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Monitor', 'Acer', '2021-02-11 03:51:43', '2021-03-03 10:31:18', NULL),
(3, 'Keyboard', 'Lenovo', '2021-02-11 03:52:18', '2021-03-03 10:31:21', NULL),
(4, 'Mouse', 'Hp', '2021-02-11 03:52:30', '2021-02-11 03:52:30', NULL),
(5, 'test', 'test', '2021-02-11 03:53:21', '2021-02-11 03:53:21', NULL),
(6, 'Keyboard', 'Lenovo', '2021-03-15 05:56:51', '2021-03-15 05:57:15', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `azhrms_company_country`
--

CREATE TABLE `azhrms_company_country` (
  `id` int NOT NULL,
  `country_name` varchar(200) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL ON UPDATE CURRENT_TIMESTAMP,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `azhrms_company_district`
--

CREATE TABLE `azhrms_company_district` (
  `id` int NOT NULL,
  `district_name` varchar(250) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `state_id` int DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL ON UPDATE CURRENT_TIMESTAMP,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `azhrms_company_district`
--

INSERT INTO `azhrms_company_district` (`id`, `district_name`, `state_id`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Anantapur', 1, '2021-03-02 16:48:41', '2021-03-02 16:48:41', NULL),
(2, 'Chittoor', 1, '2021-03-02 16:48:42', '2021-03-02 16:48:42', NULL),
(3, 'East Godavari', 1, '2021-03-02 16:48:42', '2021-03-02 16:48:42', NULL),
(4, 'Guntur', 1, '2021-03-02 16:48:42', '2021-03-02 16:48:42', NULL),
(5, 'Kadapa', 1, '2021-03-02 16:48:42', '2021-03-02 16:48:42', NULL),
(6, 'Krishna', 1, '2021-03-02 16:48:42', '2021-03-02 16:48:42', NULL),
(7, 'Kurnool', 1, '2021-03-02 16:48:42', '2021-03-02 16:48:42', NULL),
(8, 'Prakasam', 1, '2021-03-02 16:48:42', '2021-03-02 16:48:42', NULL),
(9, 'Sri Potti Sriramulu Nellore', 1, '2021-03-02 16:48:42', '2021-03-02 16:48:42', NULL),
(10, 'Srikakulam', 1, '2021-03-02 16:48:42', '2021-03-02 16:48:42', NULL),
(11, 'Visakhapatnam', 1, '2021-03-02 16:48:42', '2021-03-02 16:48:42', NULL),
(12, 'Vizianagaram', 1, '2021-03-02 16:48:42', '2021-03-02 16:48:42', NULL),
(13, 'West Godavari', 1, '2021-03-02 16:48:42', '2021-03-02 16:48:42', NULL),
(14, 'Anjaw', 2, '2021-03-02 16:48:42', '2021-03-02 16:48:42', NULL),
(15, 'Changlang', 2, '2021-03-02 16:48:43', '2021-03-02 16:48:43', NULL),
(16, 'East Kameng', 2, '2021-03-02 16:48:43', '2021-03-02 16:48:43', NULL),
(17, 'East Siang', 2, '2021-03-02 16:48:43', '2021-03-02 16:48:43', NULL),
(18, 'Kamle', 2, '2021-03-02 16:48:43', '2021-03-02 16:48:43', NULL),
(19, 'Kra Daadi', 2, '2021-03-02 16:48:43', '2021-03-02 16:48:43', NULL),
(20, 'Kurung Kumey', 2, '2021-03-02 16:48:43', '2021-03-02 16:48:43', NULL),
(21, 'Lepa Rada', 2, '2021-03-02 16:48:43', '2021-03-02 16:48:43', NULL),
(22, 'Lohit', 2, '2021-03-02 16:48:43', '2021-03-02 16:48:43', NULL),
(23, 'Longding', 2, '2021-03-02 16:48:43', '2021-03-02 16:48:43', NULL),
(24, 'Lower Dibang Valley', 2, '2021-03-02 16:48:43', '2021-03-02 16:48:43', NULL),
(25, 'Lower Siang', 2, '2021-03-02 16:48:43', '2021-03-02 16:48:43', NULL),
(26, 'Lower Subansiri', 2, '2021-03-02 16:48:43', '2021-03-02 16:48:43', NULL),
(27, 'Namsai', 2, '2021-03-02 16:48:43', '2021-03-02 16:48:43', NULL),
(28, 'Pakke-Kessang', 2, '2021-03-02 16:48:44', '2021-03-02 16:48:44', NULL),
(29, 'Papum Pare', 2, '2021-03-02 16:48:44', '2021-03-02 16:48:44', NULL),
(30, 'Shi Yomi', 2, '2021-03-02 16:48:44', '2021-03-02 16:48:44', NULL),
(31, 'Siang', 2, '2021-03-02 16:48:44', '2021-03-02 16:48:44', NULL),
(32, 'Tawang', 2, '2021-03-02 16:48:44', '2021-03-02 16:48:44', NULL),
(33, 'Tirap', 2, '2021-03-02 16:48:44', '2021-03-02 16:48:44', NULL),
(34, 'Upper Dibang Valley', 2, '2021-03-02 16:48:44', '2021-03-02 16:48:44', NULL),
(35, 'Upper Siang', 2, '2021-03-02 16:48:44', '2021-03-02 16:48:44', NULL),
(36, 'Upper Subansiri', 2, '2021-03-02 16:48:44', '2021-03-02 16:48:44', NULL),
(37, 'West Kameng', 2, '2021-03-02 16:48:44', '2021-03-02 16:48:44', NULL),
(38, 'West Siang', 2, '2021-03-02 16:48:44', '2021-03-02 16:48:44', NULL),
(39, 'Baksa', 3, '2021-03-02 16:48:44', '2021-03-02 16:48:44', NULL),
(40, 'Bajali', 3, '2021-03-02 16:48:44', '2021-03-02 16:48:44', NULL),
(41, 'Barpeta', 3, '2021-03-02 16:48:44', '2021-03-02 16:48:44', NULL),
(42, 'Bishwanath', 3, '2021-03-02 16:48:44', '2021-03-02 16:48:44', NULL),
(43, 'Bongaigaon', 3, '2021-03-02 16:48:44', '2021-03-02 16:48:44', NULL),
(44, 'Cachar', 3, '2021-03-02 16:48:45', '2021-03-02 16:48:45', NULL),
(45, 'Charaideo', 3, '2021-03-02 16:48:45', '2021-03-02 16:48:45', NULL),
(46, 'Chirang', 3, '2021-03-02 16:48:45', '2021-03-02 16:48:45', NULL),
(47, 'Darrang', 3, '2021-03-02 16:48:45', '2021-03-02 16:48:45', NULL),
(48, 'Dhemaji', 3, '2021-03-02 16:48:45', '2021-03-02 16:48:45', NULL),
(49, 'Dhubri', 3, '2021-03-02 16:48:45', '2021-03-02 16:48:45', NULL),
(50, 'Dibrugarh', 3, '2021-03-02 16:48:45', '2021-03-02 16:48:45', NULL),
(51, 'Dima Hasao', 3, '2021-03-02 16:48:45', '2021-03-02 16:48:45', NULL),
(52, 'Goalpara', 3, '2021-03-02 16:48:45', '2021-03-02 16:48:45', NULL),
(53, 'Golaghat', 3, '2021-03-02 16:48:45', '2021-03-02 16:48:45', NULL),
(54, 'Hailakandi', 3, '2021-03-02 16:48:45', '2021-03-02 16:48:45', NULL),
(55, 'Hojai', 3, '2021-03-02 16:48:45', '2021-03-02 16:48:45', NULL),
(56, 'Jorhat', 3, '2021-03-02 16:48:45', '2021-03-02 16:48:45', NULL),
(57, 'Kamrup', 3, '2021-03-02 16:48:45', '2021-03-02 16:48:45', NULL),
(58, 'Kamrup Metropolitan', 3, '2021-03-02 16:48:45', '2021-03-02 16:48:45', NULL),
(59, 'Karbi Anglong', 3, '2021-03-02 16:48:46', '2021-03-02 16:48:46', NULL),
(60, 'Karimganj', 3, '2021-03-02 16:48:46', '2021-03-02 16:48:46', NULL),
(61, 'Kokrajhar', 3, '2021-03-02 16:48:46', '2021-03-02 16:48:46', NULL),
(62, 'Lakhimpur', 3, '2021-03-02 16:48:46', '2021-03-02 16:48:46', NULL),
(63, 'Majuli', 3, '2021-03-02 16:48:46', '2021-03-02 16:48:46', NULL),
(64, 'Morigaon', 3, '2021-03-02 16:48:46', '2021-03-02 16:48:46', NULL),
(65, 'Nagaon', 3, '2021-03-02 16:48:46', '2021-03-02 16:48:46', NULL),
(66, 'Nalbari', 3, '2021-03-02 16:48:46', '2021-03-02 16:48:46', NULL),
(67, 'Sivasagar', 3, '2021-03-02 16:48:46', '2021-03-02 16:48:46', NULL),
(68, 'South Salmara', 3, '2021-03-02 16:48:46', '2021-03-02 16:48:46', NULL),
(69, 'Sonitpur', 3, '2021-03-02 16:48:46', '2021-03-02 16:48:46', NULL),
(70, 'Tinsukia', 3, '2021-03-02 16:48:46', '2021-03-02 16:48:46', NULL),
(71, 'Udalguri', 3, '2021-03-02 16:48:46', '2021-03-02 16:48:46', NULL),
(72, 'West Karbi Anglong', 3, '2021-03-02 16:48:46', '2021-03-02 16:48:46', NULL),
(73, 'Alipurduars', 28, '2021-03-02 16:48:46', '2021-03-05 06:34:34', NULL),
(74, 'Bankura', 28, '2021-03-02 16:48:46', '2021-03-02 16:48:46', NULL),
(75, 'Paschim Bardhaman', 28, '2021-03-02 16:48:47', '2021-03-02 16:48:47', NULL),
(76, 'Purba Bardhaman', 28, '2021-03-02 16:48:47', '2021-03-02 16:48:47', NULL),
(77, 'Birbhum', 28, '2021-03-02 16:48:47', '2021-03-02 16:48:47', NULL),
(78, 'Cooch Behar', 28, '2021-03-02 16:48:47', '2021-03-02 16:48:47', NULL),
(79, 'Dakshin Dinajpur', 28, '2021-03-02 16:48:47', '2021-03-02 16:48:47', NULL),
(80, 'Darjeeling', 28, '2021-03-02 16:48:47', '2021-03-02 16:48:47', NULL),
(81, 'Hooghly', 28, '2021-03-02 16:48:47', '2021-03-02 16:48:47', NULL),
(82, 'Howrah', 28, '2021-03-02 16:48:47', '2021-03-02 16:48:47', NULL),
(83, 'Jalpaiguri', 28, '2021-03-02 16:48:47', '2021-03-02 16:48:47', NULL),
(84, 'Jhargram', 28, '2021-03-02 16:48:47', '2021-03-02 16:48:47', NULL),
(85, 'Kalimpong', 28, '2021-03-02 16:48:47', '2021-03-02 16:48:47', NULL),
(86, 'Kolkata', 28, '2021-03-02 16:48:47', '2021-03-02 16:48:47', NULL),
(87, 'Maldah', 28, '2021-03-02 16:48:47', '2021-03-02 16:48:47', NULL),
(88, 'Murshidabad', 28, '2021-03-02 16:48:47', '2021-03-02 16:48:47', NULL),
(89, 'Nadia', 28, '2021-03-02 16:48:47', '2021-03-02 16:48:47', NULL),
(90, 'North 24 Parganas', 28, '2021-03-02 16:48:47', '2021-03-02 16:48:47', NULL),
(91, 'Paschim Medinipur', 28, '2021-03-02 16:48:48', '2021-03-02 16:48:48', NULL),
(92, 'Purba Medinipur', 28, '2021-03-02 16:48:48', '2021-03-02 16:48:48', NULL),
(93, 'Purulia', 28, '2021-03-02 16:48:48', '2021-03-02 16:48:48', NULL),
(94, 'South 24 Parganas', 28, '2021-03-02 16:48:48', '2021-03-02 16:48:48', NULL),
(95, 'Uttar Dinajpur', 28, '2021-03-02 16:48:48', '2021-03-02 16:48:48', NULL),
(96, 'Mumbai', 14, '2021-03-05 06:24:42', '2021-03-05 11:56:54', NULL),
(97, 'Pune', 14, '2021-03-05 06:26:27', '2021-03-05 06:26:27', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `azhrms_company_gen_info`
--

CREATE TABLE `azhrms_company_gen_info` (
  `id` int NOT NULL,
  `c_name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `res_company_name` varchar(4) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tax_id` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `registration_number` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `company_logo` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT '0',
  `note` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL ON UPDATE CURRENT_TIMESTAMP,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `azhrms_company_gen_info`
--

INSERT INTO `azhrms_company_gen_info` (`id`, `c_name`, `res_company_name`, `tax_id`, `registration_number`, `company_logo`, `note`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Azure Software pvt ltd', 'aspl', 'Chert123Er', '123456', '1617261123.jpg', NULL, '2021-04-01 01:42:03', '2021-04-01 01:42:03', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `azhrms_company_location`
--

CREATE TABLE `azhrms_company_location` (
  `id` int NOT NULL,
  `l_name` varchar(110) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `operational_company_id` int DEFAULT NULL,
  `state` int DEFAULT NULL,
  `district` int DEFAULT NULL,
  `city` varchar(60) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `zip_code` varchar(35) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone` varchar(35) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `country_code` int DEFAULT NULL,
  `area_code` int DEFAULT NULL,
  `fax` varchar(35) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `notes` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `azhrms_company_location`
--

INSERT INTO `azhrms_company_location` (`id`, `l_name`, `operational_company_id`, `state`, `district`, `city`, `address`, `zip_code`, `phone`, `country_code`, `area_code`, `fax`, `notes`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Kolkata', 1, 28, 86, 'Kolkata', '95 SDF Building', '700091', '6291789452', 91, NULL, NULL, NULL, '2021-04-01 01:43:03', '2021-04-01 01:43:03', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `azhrms_company_location_department`
--

CREATE TABLE `azhrms_company_location_department` (
  `id` int NOT NULL,
  `d_name` varchar(110) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `operational_company_location_id` int DEFAULT NULL,
  `type` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(35) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `country_code` int DEFAULT NULL,
  `area_code` int DEFAULT NULL,
  `fax` varchar(35) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `notes` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL ON UPDATE CURRENT_TIMESTAMP,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `azhrms_company_location_department`
--

INSERT INTO `azhrms_company_location_department` (`id`, `d_name`, `operational_company_location_id`, `type`, `phone`, `country_code`, `area_code`, `fax`, `notes`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Accounts', 1, 'Department', '6291789452', 91, 33, NULL, NULL, '2021-04-01 01:43:46', '2021-04-01 01:43:46', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `azhrms_company_state`
--

CREATE TABLE `azhrms_company_state` (
  `id` int NOT NULL,
  `state_name` varchar(200) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `sortname` varchar(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL ON UPDATE CURRENT_TIMESTAMP,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `azhrms_company_state`
--

INSERT INTO `azhrms_company_state` (`id`, `state_name`, `sortname`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Andhra Pradesh', NULL, '2021-03-02 16:47:53', '2021-03-02 16:47:53', NULL),
(2, 'Arunachal Pradesh', NULL, '2021-03-02 16:47:53', '2021-03-02 16:47:53', NULL),
(3, 'Assam', NULL, '2021-03-02 16:47:53', '2021-03-02 16:47:53', NULL),
(4, 'Bihar', NULL, '2021-03-02 16:47:53', '2021-03-02 16:47:53', NULL),
(5, 'Chhattisgarh', NULL, '2021-03-02 16:47:53', '2021-03-02 16:47:53', NULL),
(6, 'Goa', NULL, '2021-03-02 16:47:53', '2021-03-02 16:47:53', NULL),
(7, 'Gujarat', NULL, '2021-03-02 16:47:53', '2021-03-02 16:47:53', NULL),
(8, 'Haryana', NULL, '2021-03-02 16:47:53', '2021-03-02 16:47:53', NULL),
(9, 'Himachal Pradesh', NULL, '2021-03-02 16:47:53', '2021-03-02 16:47:53', NULL),
(10, 'Jharkhand', NULL, '2021-03-02 16:47:54', '2021-03-02 16:47:54', NULL),
(11, 'Karnataka', NULL, '2021-03-02 16:47:54', '2021-03-02 16:47:54', NULL),
(12, 'Kerala', NULL, '2021-03-02 16:47:54', '2021-03-02 16:47:54', NULL),
(13, 'Madhya Pradesh', NULL, '2021-03-02 16:47:54', '2021-03-02 16:47:54', NULL),
(14, 'Maharashtra', NULL, '2021-03-02 16:47:54', '2021-03-02 16:47:54', NULL),
(15, 'Manipur', NULL, '2021-03-02 16:47:54', '2021-03-02 16:47:54', NULL),
(16, 'Meghalaya', NULL, '2021-03-02 16:47:54', '2021-03-02 16:47:54', NULL),
(17, 'Mizoram', NULL, '2021-03-02 16:47:54', '2021-03-02 16:47:54', NULL),
(18, 'Nagaland', NULL, '2021-03-02 16:47:54', '2021-03-02 16:47:54', NULL),
(19, 'Odisha', NULL, '2021-03-02 16:47:54', '2021-03-02 16:47:54', NULL),
(20, 'Punjab', NULL, '2021-03-02 16:47:55', '2021-03-02 16:47:55', NULL),
(21, 'Rajasthan', NULL, '2021-03-02 16:47:55', '2021-03-02 16:47:55', NULL),
(22, 'Sikkim', NULL, '2021-03-02 16:47:55', '2021-03-02 16:47:55', NULL),
(23, 'Tamil Nadu', NULL, '2021-03-02 16:47:55', '2021-03-02 16:47:55', NULL),
(24, 'Telangana', NULL, '2021-03-02 16:47:55', '2021-03-02 16:47:55', NULL),
(25, 'Tripura', NULL, '2021-03-02 16:47:55', '2021-03-02 16:47:55', NULL),
(26, 'Uttarakhand', NULL, '2021-03-02 16:47:55', '2021-03-02 16:47:55', NULL),
(27, 'Uttar Pradesh', NULL, '2021-03-02 16:47:55', '2021-03-02 16:47:55', NULL),
(28, 'West Bengal', NULL, '2021-03-02 16:47:55', '2021-03-02 16:47:55', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `azhrms_complain`
--

CREATE TABLE `azhrms_complain` (
  `id` int NOT NULL,
  `emp_id` int NOT NULL,
  `complain_against_id` int NOT NULL,
  `complain` varchar(1000) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `notes` varchar(200) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `azhrms_corporateactiontypemasters`
--

CREATE TABLE `azhrms_corporateactiontypemasters` (
  `id` int NOT NULL,
  `description` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `azhrms_corporateactiontypemasters`
--

INSERT INTO `azhrms_corporateactiontypemasters` (`id`, `description`, `created_at`, `updated_at`) VALUES
(1, '1st Warning', '2021-01-06 14:12:12', NULL),
(2, '2nd Warning', '2021-01-06 14:12:12', NULL),
(3, '3rd Warning', '2021-01-06 14:12:12', NULL),
(4, 'Suspension for 1 Week', '2021-01-06 14:12:12', NULL),
(5, 'Show Cause', '2021-01-06 14:12:12', NULL),
(6, 'Promotions', '2021-01-06 14:12:12', NULL),
(7, 'Awarded', '2021-01-06 14:12:12', NULL),
(8, 'Resigned', '2021-01-06 14:12:12', NULL),
(9, 'Deceased', '2021-01-06 14:12:12', NULL),
(10, 'Physically Disabled/Compensated', '2021-01-06 14:12:13', NULL),
(11, 'Laid-off', '2021-01-06 14:12:13', NULL),
(12, 'Dismissed1', '2021-01-06 14:12:13', '2021-01-11 06:12:48');

-- --------------------------------------------------------

--
-- Table structure for table `azhrms_crm`
--

CREATE TABLE `azhrms_crm` (
  `id` int NOT NULL,
  `crm` varchar(200) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `azhrms_daily_report`
--

CREATE TABLE `azhrms_daily_report` (
  `id` int NOT NULL,
  `emp_id` int NOT NULL,
  `report_date` timestamp NOT NULL,
  `report_time` varchar(200) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `job_type_id` int NOT NULL,
  `job_category_id` int NOT NULL,
  `crm_id` int NOT NULL,
  `words` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `job_description` varchar(200) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `job_id` int NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `azhrms_district`
--

CREATE TABLE `azhrms_district` (
  `district_code` varchar(13) NOT NULL DEFAULT '',
  `district_name` varchar(50) DEFAULT NULL,
  `province_code` varchar(13) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `azhrms_education`
--

CREATE TABLE `azhrms_education` (
  `id` int NOT NULL,
  `name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL ON UPDATE CURRENT_TIMESTAMP,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `azhrms_employee`
--

CREATE TABLE `azhrms_employee` (
  `id` int NOT NULL,
  `emp_code` varchar(250) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `emp_lastname` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `emp_firstname` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `emp_middle_name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `emp_nick_name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT '',
  `emp_birthday` date DEFAULT NULL,
  `emp_skill_id` int DEFAULT NULL,
  `emp_education_id` int DEFAULT NULL,
  `operational_company_id` int DEFAULT NULL,
  `operational_company_location_id` int DEFAULT NULL,
  `operational_company_loc_dept_id` int DEFAULT NULL,
  `emp_gender` varchar(15) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `emp_marital_status` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `emp_religion_id` int DEFAULT NULL,
  `emp_nationality_id` int DEFAULT NULL,
  `emp_pan_num` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `emp_aadhar_num` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `emp_other_id` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `emp_bloodgroup` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `emp_street1` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `emp_street2` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `city_code` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `state_code` int DEFAULT NULL,
  `district_code` int DEFAULT NULL,
  `emp_pincode` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `emp_hm_telephone` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `country_code_home` int DEFAULT NULL,
  `area_code_home` int DEFAULT NULL,
  `emp_mobile` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `country_code_mobile` int DEFAULT NULL,
  `area_code_mobile` int DEFAULT NULL,
  `emp_work_telephone` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `work_country_code` int DEFAULT NULL,
  `work_area_code` int DEFAULT NULL,
  `emp_work_email` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `emp_status_type` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `joined_date` date DEFAULT NULL,
  `related_person` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `emp_oth_email` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `emp_status` int DEFAULT NULL,
  `emp_img` varchar(250) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT '0',
  `emp_language` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `reporting_to` int DEFAULT NULL,
  `shift` int DEFAULT NULL,
  `designation` int DEFAULT NULL,
  `job_role` int DEFAULT NULL,
  `org_age` varchar(250) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `emp_food_habit` varchar(15) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `blood_doc` varchar(250) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `pan_doc` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `azhrms_employee`
--

INSERT INTO `azhrms_employee` (`id`, `emp_code`, `emp_lastname`, `emp_firstname`, `emp_middle_name`, `emp_nick_name`, `emp_birthday`, `emp_skill_id`, `emp_education_id`, `operational_company_id`, `operational_company_location_id`, `operational_company_loc_dept_id`, `emp_gender`, `emp_marital_status`, `emp_religion_id`, `emp_nationality_id`, `emp_pan_num`, `emp_aadhar_num`, `emp_other_id`, `emp_bloodgroup`, `emp_street1`, `emp_street2`, `city_code`, `state_code`, `district_code`, `emp_pincode`, `emp_hm_telephone`, `country_code_home`, `area_code_home`, `emp_mobile`, `country_code_mobile`, `area_code_mobile`, `emp_work_telephone`, `work_country_code`, `work_area_code`, `emp_work_email`, `emp_status_type`, `joined_date`, `related_person`, `emp_oth_email`, `emp_status`, `emp_img`, `emp_language`, `reporting_to`, `shift`, `designation`, `job_role`, `org_age`, `emp_food_habit`, `blood_doc`, `pan_doc`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'aspl-2021-000', 'Research', 'Royal', NULL, 'Superadmin', NULL, NULL, NULL, 1, 1, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'superadmin@gmail.com', NULL, NULL, NULL, NULL, NULL, '0', NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, '2021-04-01 07:10:31', '2021-04-01 07:16:15', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `azhrms_employee_assets`
--

CREATE TABLE `azhrms_employee_assets` (
  `id` int NOT NULL,
  `emp_id` int NOT NULL,
  `property_name` varchar(200) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `property_details` varchar(250) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `giving_date` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `return_date` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `return_property_conditions` varchar(250) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `issuer` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `azhrms_employee_assets`
--

INSERT INTO `azhrms_employee_assets` (`id`, `emp_id`, `property_name`, `property_details`, `giving_date`, `return_date`, `return_property_conditions`, `issuer`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 1, NULL, NULL, NULL, NULL, NULL, NULL, '2021-04-01 07:20:00', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `azhrms_employee_attandance`
--

CREATE TABLE `azhrms_employee_attandance` (
  `id` int NOT NULL,
  `emp_id` int NOT NULL,
  `date` datetime NOT NULL,
  `in_time` timestamp NOT NULL,
  `out_time` timestamp NOT NULL,
  `shift_id` int NOT NULL,
  `total_duration` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `status` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `remarks` varchar(200) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `status_flag` int NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `azhrms_employee_bank_details`
--

CREATE TABLE `azhrms_employee_bank_details` (
  `id` int NOT NULL,
  `emp_id` int NOT NULL,
  `acnt_holder_name` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `bank_name` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `branch_name` varchar(200) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `account_number` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `neft_code` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `ifsc_code` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `azhrms_employee_bank_details`
--

INSERT INTO `azhrms_employee_bank_details` (`id`, `emp_id`, `acnt_holder_name`, `bank_name`, `branch_name`, `account_number`, `neft_code`, `ifsc_code`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 1, NULL, NULL, NULL, NULL, NULL, NULL, '2021-04-01 07:18:42', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `azhrms_employee_edu_details`
--

CREATE TABLE `azhrms_employee_edu_details` (
  `id` int NOT NULL,
  `emp_id` int NOT NULL,
  `emp_education_id` int DEFAULT NULL,
  `ins_name` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `degree` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `grade` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `year` int DEFAULT NULL,
  `notes` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `edu_doc` varchar(200) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `azhrms_employee_edu_details`
--

INSERT INTO `azhrms_employee_edu_details` (`id`, `emp_id`, `emp_education_id`, `ins_name`, `degree`, `grade`, `year`, `notes`, `edu_doc`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-04-01 07:17:38', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `azhrms_employee_feedback`
--

CREATE TABLE `azhrms_employee_feedback` (
  `id` int NOT NULL,
  `emp_id` int NOT NULL,
  `feedback_to_id` int NOT NULL,
  `feedback_category` int NOT NULL,
  `feedback_type` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `feedback_comment` varchar(600) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `azhrms_employee_language`
--

CREATE TABLE `azhrms_employee_language` (
  `id` int NOT NULL,
  `lng_name` varchar(200) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL ON UPDATE CURRENT_TIMESTAMP,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `azhrms_employee_language`
--

INSERT INTO `azhrms_employee_language` (`id`, `lng_name`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'English', '2021-02-10 05:25:10', '2021-02-10 05:25:10', NULL),
(2, 'Bengali', '2021-02-10 05:25:20', '2021-02-10 05:25:20', NULL),
(3, 'Hindi', '2021-02-10 05:25:28', '2021-02-10 05:25:28', NULL),
(4, 'test12', '2021-02-10 05:25:34', '2021-03-03 10:36:54', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `azhrms_employee_promotion`
--

CREATE TABLE `azhrms_employee_promotion` (
  `id` int NOT NULL,
  `emp_id` int NOT NULL,
  `promotion_date` varchar(200) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `effective_from` varchar(200) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `last_designation` varchar(200) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `last_salary` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `letters` varchar(250) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `azhrms_employee_promotion`
--

INSERT INTO `azhrms_employee_promotion` (`id`, `emp_id`, `promotion_date`, `effective_from`, `last_designation`, `last_salary`, `letters`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 1, NULL, NULL, NULL, NULL, NULL, '2021-04-01 07:20:28', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `azhrms_employee_salary`
--

CREATE TABLE `azhrms_employee_salary` (
  `id` int NOT NULL,
  `emp_id` int NOT NULL,
  `committed_amount` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `ctc_per_month` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `esi_number` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `pf_uan_no` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `pf_no` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `ctc_per_annum` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `payroll_org` varchar(200) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `pf_effective_date` varchar(200) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `azhrms_employee_salary`
--

INSERT INTO `azhrms_employee_salary` (`id`, `emp_id`, `committed_amount`, `ctc_per_month`, `esi_number`, `pf_uan_no`, `pf_no`, `ctc_per_annum`, `payroll_org`, `pf_effective_date`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-04-01 07:19:31', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `azhrms_employee_skill_details`
--

CREATE TABLE `azhrms_employee_skill_details` (
  `id` int NOT NULL,
  `emp_id` int NOT NULL,
  `emp_skill_id` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `skill_grade` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `azhrms_employee_skill_details`
--

INSERT INTO `azhrms_employee_skill_details` (`id`, `emp_id`, `emp_skill_id`, `skill_grade`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 1, NULL, NULL, '2021-04-01 07:18:20', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `azhrms_employee_type`
--

CREATE TABLE `azhrms_employee_type` (
  `id` int NOT NULL,
  `emp_type_name` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL ON UPDATE CURRENT_TIMESTAMP,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `azhrms_employee_type`
--

INSERT INTO `azhrms_employee_type` (`id`, `emp_type_name`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Permanent', '2021-02-04 04:53:07', '2021-04-01 06:44:18', NULL),
(2, 'Provident', '2021-02-04 04:54:29', '2021-04-01 06:44:26', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `azhrms_employee_warning`
--

CREATE TABLE `azhrms_employee_warning` (
  `id` int NOT NULL,
  `emp_id` int NOT NULL,
  `warning_emp_name` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `warning_given_id` int NOT NULL,
  `issuer_name` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `warning_header` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `reason` varchar(10000) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `date` timestamp NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `azhrms_employee_work_shift`
--

CREATE TABLE `azhrms_employee_work_shift` (
  `work_shift_id` int NOT NULL,
  `emp_id` int NOT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `deleted_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `azhrms_emp_attachment`
--

CREATE TABLE `azhrms_emp_attachment` (
  `emp_number` int NOT NULL DEFAULT '0',
  `eattach_id` int NOT NULL DEFAULT '0',
  `eattach_desc` varchar(200) DEFAULT NULL,
  `eattach_filename` varchar(100) DEFAULT NULL,
  `eattach_size` int DEFAULT '0',
  `eattach_attachment` mediumblob,
  `eattach_type` varchar(200) DEFAULT NULL,
  `screen` varchar(100) DEFAULT '',
  `attached_by` int DEFAULT NULL,
  `attached_by_name` varchar(200) DEFAULT NULL,
  `attached_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `azhrms_emp_basicsalary`
--

CREATE TABLE `azhrms_emp_basicsalary` (
  `id` int NOT NULL,
  `emp_number` int NOT NULL DEFAULT '0',
  `sal_grd_code` int DEFAULT NULL,
  `currency_id` varchar(6) NOT NULL DEFAULT '',
  `ebsal_basic_salary` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT NULL,
  `payperiod_code` varchar(13) DEFAULT NULL,
  `salary_component` varchar(100) DEFAULT NULL,
  `comments` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `azhrms_emp_children`
--

CREATE TABLE `azhrms_emp_children` (
  `emp_number` int NOT NULL DEFAULT '0',
  `ec_seqno` decimal(2,0) NOT NULL DEFAULT '0',
  `ec_name` varchar(100) DEFAULT '',
  `ec_date_of_birth` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `azhrms_emp_contract_extend`
--

CREATE TABLE `azhrms_emp_contract_extend` (
  `emp_number` int NOT NULL DEFAULT '0',
  `econ_extend_id` decimal(10,0) NOT NULL DEFAULT '0',
  `econ_extend_start_date` datetime DEFAULT NULL,
  `econ_extend_end_date` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `azhrms_emp_dependents`
--

CREATE TABLE `azhrms_emp_dependents` (
  `emp_number` int NOT NULL DEFAULT '0',
  `ed_seqno` decimal(2,0) NOT NULL DEFAULT '0',
  `ed_name` varchar(100) DEFAULT '',
  `ed_relationship_type` enum('child','other') DEFAULT NULL,
  `ed_relationship` varchar(100) DEFAULT '',
  `ed_date_of_birth` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `azhrms_exam_score`
--

CREATE TABLE `azhrms_exam_score` (
  `id` int NOT NULL,
  `emp_id` int NOT NULL,
  `training_given_by_id` int NOT NULL,
  `exam_score_date` timestamp NOT NULL,
  `exam_score_time` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `subject_id` int NOT NULL,
  `topics` varchar(200) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `duration_from` timestamp NOT NULL,
  `duration_to` timestamp NOT NULL,
  `exam_score` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `azhrms_holiday`
--

CREATE TABLE `azhrms_holiday` (
  `id` int NOT NULL,
  `description` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `date` date DEFAULT NULL,
  `recurring` tinyint DEFAULT '0',
  `length` int UNSIGNED DEFAULT NULL,
  `operational_company_id` int DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `azhrms_jobcategory`
--

CREATE TABLE `azhrms_jobcategory` (
  `id` int NOT NULL,
  `job_category` varchar(200) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `azhrms_jobtype`
--

CREATE TABLE `azhrms_jobtype` (
  `id` int NOT NULL,
  `job_type` varchar(200) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `azhrms_leave_entitlement`
--

CREATE TABLE `azhrms_leave_entitlement` (
  `id` int UNSIGNED NOT NULL,
  `company_id` int DEFAULT NULL,
  `period` varchar(250) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `emp_number` int DEFAULT NULL,
  `no_of_days` int NOT NULL,
  `days_used` decimal(8,4) NOT NULL DEFAULT '0.0000',
  `leave_type_id` int UNSIGNED NOT NULL,
  `note` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  `created_by_id` bigint UNSIGNED DEFAULT NULL,
  `created_by_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `azhrms_leave_period_history`
--

CREATE TABLE `azhrms_leave_period_history` (
  `id` int NOT NULL,
  `leave_period_start_month` int DEFAULT NULL,
  `leave_period_start_date` date DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `azhrms_leave_type`
--

CREATE TABLE `azhrms_leave_type` (
  `id` int UNSIGNED NOT NULL,
  `name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `deleted` tinyint NOT NULL DEFAULT '0',
  `exclude_in_reports_if_no_entitlement` tinyint DEFAULT '0',
  `operational_company_id` int DEFAULT NULL,
  `operational_company_location_id` int DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `azhrms_leave_type`
--

INSERT INTO `azhrms_leave_type` (`id`, `name`, `deleted`, `exclude_in_reports_if_no_entitlement`, `operational_company_id`, `operational_company_location_id`, `created_at`, `updated_at`) VALUES
(1, 'CL', 0, 1, NULL, NULL, '2021-01-18 05:51:06', '2021-02-18 15:36:25'),
(2, 'NPL', 0, NULL, NULL, NULL, '2021-01-18 07:15:26', '2021-02-18 09:08:35'),
(3, 'Long Leave', 0, NULL, NULL, NULL, '2021-01-19 05:04:58', '2021-02-18 15:36:29'),
(4, 'Educational Leave', 0, 1, NULL, NULL, '2021-01-29 06:00:42', '2021-02-18 09:08:47'),
(5, 'Medical Leave', 0, NULL, NULL, NULL, '2021-01-29 06:00:58', '2021-02-18 09:09:04');

-- --------------------------------------------------------

--
-- Table structure for table `azhrms_nationalities`
--

CREATE TABLE `azhrms_nationalities` (
  `id` int NOT NULL,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `azhrms_nationalities`
--

INSERT INTO `azhrms_nationalities` (`id`, `name`, `created_at`, `updated_at`, `deleted_at`) VALUES
(4, 'India', '2021-01-27 01:31:00', '2021-03-03 10:40:51', NULL),
(5, 'Bhutan', '2021-01-27 04:04:44', '2021-01-27 04:04:44', NULL),
(6, 'USA', '2021-03-01 04:34:43', '2021-03-01 04:34:43', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `azhrms_religion`
--

CREATE TABLE `azhrms_religion` (
  `id` int NOT NULL,
  `name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL ON UPDATE CURRENT_TIMESTAMP,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `azhrms_religion`
--

INSERT INTO `azhrms_religion` (`id`, `name`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Hindu', '2021-01-27 06:54:42', '0000-00-00 00:00:00', NULL),
(2, 'Muslim', '2021-01-27 06:54:42', '0000-00-00 00:00:00', NULL),
(3, 'Christian', '2021-01-27 06:54:42', '0000-00-00 00:00:00', NULL),
(4, 'Sikh', '2021-01-27 06:54:42', '0000-00-00 00:00:00', NULL),
(5, 'Buddhist', '2021-01-27 06:54:42', '0000-00-00 00:00:00', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `azhrms_skill`
--

CREATE TABLE `azhrms_skill` (
  `id` int NOT NULL,
  `name` varchar(120) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `azhrms_subject`
--

CREATE TABLE `azhrms_subject` (
  `id` int NOT NULL,
  `subject` varchar(200) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `azhrms_subject`
--

INSERT INTO `azhrms_subject` (`id`, `subject`, `created_at`, `updated_at`) VALUES
(1, 'Physics', '2021-03-16 10:21:53', '2021-03-16 10:21:53'),
(2, 'Chemistry', '2021-03-16 10:22:02', '2021-03-16 10:22:02'),
(3, 'Math', '2021-03-16 10:22:10', '2021-03-16 10:22:10'),
(4, 'Biology', '2021-03-16 10:22:17', '2021-03-16 10:22:22');

-- --------------------------------------------------------

--
-- Table structure for table `azhrms_time_tracker_review`
--

CREATE TABLE `azhrms_time_tracker_review` (
  `id` int NOT NULL,
  `emp_id` int NOT NULL,
  `review` varchar(200) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `azhrms_training`
--

CREATE TABLE `azhrms_training` (
  `id` int NOT NULL,
  `emp_id` int NOT NULL,
  `training_given_by_id` int NOT NULL,
  `training_date` timestamp NOT NULL,
  `training_time` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `subject_id` int NOT NULL,
  `topics` varchar(200) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `duration_from` timestamp NOT NULL,
  `duration_to` timestamp NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `azhrms_user_role`
--

CREATE TABLE `azhrms_user_role` (
  `id` int NOT NULL,
  `name` varchar(255) NOT NULL,
  `display_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT '0',
  `role_hierarchy` tinyint(1) DEFAULT '0',
  `is_predefined` tinyint(1) DEFAULT '0',
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `azhrms_user_role`
--

INSERT INTO `azhrms_user_role` (`id`, `name`, `display_name`, `role_hierarchy`, `is_predefined`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Superadmin', 'Superadmin', 1, 1, '2021-01-04 09:29:59', '2021-03-03 10:42:00', NULL),
(2, 'Admin', 'Admin', 2, 1, '2021-01-04 09:29:59', '2021-03-03 10:42:03', NULL),
(3, 'Cluster Head', 'Cluster Head', 3, 1, '2021-01-04 09:29:59', '2021-03-03 10:42:06', NULL),
(4, 'ECRM', 'ECRM', 4, 1, '2021-01-04 09:29:59', '2021-03-03 10:42:09', NULL),
(5, 'BDM', 'BDM', 5, 1, '2021-01-04 09:29:59', '2021-03-03 10:42:13', NULL),
(6, 'Team Leader', 'Team Leader', 6, 1, '2021-01-04 09:29:59', '2021-03-03 10:42:16', NULL),
(7, 'ACW', 'ACW', 7, 1, '2021-01-04 09:29:59', '2021-03-03 10:42:18', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `azhrms_user_role_categories`
--

CREATE TABLE `azhrms_user_role_categories` (
  `id` int NOT NULL,
  `respname` varchar(255) NOT NULL,
  `resp_display_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT '0',
  `is_assignable` tinyint(1) DEFAULT '0',
  `is_predefined` tinyint(1) DEFAULT '0',
  `role_id` int NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL ON UPDATE CURRENT_TIMESTAMP,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `azhrms_work_shift`
--

CREATE TABLE `azhrms_work_shift` (
  `id` int NOT NULL,
  `name` varchar(250) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `hours_per_day` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `start_time` time NOT NULL,
  `end_time` time NOT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `azhrms_work_week`
--

CREATE TABLE `azhrms_work_week` (
  `id` int NOT NULL,
  `operational_company_id` int DEFAULT NULL,
  `operational_company_location_id` int DEFAULT NULL,
  `operational_company_loc_dept_id` int DEFAULT NULL,
  `mon` tinyint UNSIGNED NOT NULL DEFAULT '0',
  `tue` tinyint UNSIGNED NOT NULL DEFAULT '0',
  `wed` tinyint UNSIGNED NOT NULL DEFAULT '0',
  `thu` tinyint UNSIGNED NOT NULL DEFAULT '0',
  `fri` tinyint UNSIGNED NOT NULL DEFAULT '0',
  `sat` tinyint UNSIGNED NOT NULL DEFAULT '0',
  `sun` tinyint UNSIGNED NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `azhrms_work_week`
--

INSERT INTO `azhrms_work_week` (`id`, `operational_company_id`, `operational_company_location_id`, `operational_company_loc_dept_id`, `mon`, `tue`, `wed`, `thu`, `fri`, `sat`, `sun`, `created_at`, `updated_at`) VALUES
(1, NULL, NULL, NULL, 0, 0, 0, 0, 0, 1, 2, '2021-01-18 13:47:54', '2021-01-18 08:22:04');

-- --------------------------------------------------------

--
-- Table structure for table `employee_language`
--

CREATE TABLE `employee_language` (
  `id` int NOT NULL,
  `emp_id` int NOT NULL,
  `language_id` int DEFAULT NULL,
  `proficiency` int DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `deleted_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `employee_language`
--

INSERT INTO `employee_language` (`id`, `emp_id`, `language_id`, `proficiency`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 1, NULL, NULL, '2021-04-01 07:21:02', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint UNSIGNED NOT NULL,
  `connection` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `feedback_category`
--

CREATE TABLE `feedback_category` (
  `id` int NOT NULL,
  `feedback` varchar(200) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `feedback_category`
--

INSERT INTO `feedback_category` (`id`, `feedback`, `created_at`, `updated_at`) VALUES
(1, 'PLAGIARISM', '2021-03-09 12:12:48', '2021-03-09 12:13:36'),
(2, 'BAD BEHAVIOUR', '2021-03-09 12:13:51', '2021-03-09 12:13:51'),
(3, 'AUDACITY', '2021-03-09 12:15:52', '2021-03-09 12:15:52');

-- --------------------------------------------------------

--
-- Table structure for table `imgtest`
--

CREATE TABLE `imgtest` (
  `id` int NOT NULL,
  `img` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `imgtest`
--

INSERT INTO `imgtest` (`id`, `img`, `created_at`, `updated_at`) VALUES
(1, '1612442493.jpg', '2021-02-04 07:11:33', '2021-02-04 07:11:33'),
(2, '1612442677.jpg', '2021-02-04 07:14:37', '2021-02-04 07:14:37'),
(3, '1612445152.jpg', '2021-02-04 07:55:52', '2021-02-04 07:55:52');

-- --------------------------------------------------------

--
-- Table structure for table `jobs`
--

CREATE TABLE `jobs` (
  `id` bigint UNSIGNED NOT NULL,
  `queue` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `attempts` tinyint UNSIGNED NOT NULL,
  `reserved_at` int UNSIGNED DEFAULT NULL,
  `available_at` int UNSIGNED NOT NULL,
  `created_at` int UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `jobs`
--

INSERT INTO `jobs` (`id`, `queue`, `payload`, `attempts`, `reserved_at`, `available_at`, `created_at`) VALUES
(218, 'default', '{\"uuid\":\"bf46a6e9-37e4-4771-984b-26c680fe75b4\",\"displayName\":\"App\\\\Notifications\\\\EmployeeSalaryNotification\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"maxExceptions\":null,\"backoff\":\"\",\"timeout\":null,\"retryUntil\":null,\"data\":{\"commandName\":\"Illuminate\\\\Notifications\\\\SendQueuedNotifications\",\"command\":\"O:48:\\\"Illuminate\\\\Notifications\\\\SendQueuedNotifications\\\":15:{s:11:\\\"notifiables\\\";O:45:\\\"Illuminate\\\\Contracts\\\\Database\\\\ModelIdentifier\\\":4:{s:5:\\\"class\\\";s:15:\\\"App\\\\Models\\\\User\\\";s:2:\\\"id\\\";a:1:{i:0;i:1;}s:9:\\\"relations\\\";a:0:{}s:10:\\\"connection\\\";s:5:\\\"mysql\\\";}s:12:\\\"notification\\\";O:44:\\\"App\\\\Notifications\\\\EmployeeSalaryNotification\\\":12:{s:4:\\\"user\\\";O:45:\\\"Illuminate\\\\Contracts\\\\Database\\\\ModelIdentifier\\\":4:{s:5:\\\"class\\\";s:15:\\\"App\\\\Models\\\\User\\\";s:2:\\\"id\\\";i:1;s:9:\\\"relations\\\";a:0:{}s:10:\\\"connection\\\";s:5:\\\"mysql\\\";}s:2:\\\"id\\\";s:36:\\\"cb22d01b-33e1-4ab9-af09-fbf6ccbe0a9f\\\";s:6:\\\"locale\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:19:\\\"chainCatchCallbacks\\\";N;s:5:\\\"delay\\\";N;s:11:\\\"afterCommit\\\";N;s:10:\\\"middleware\\\";a:0:{}s:7:\\\"chained\\\";a:0:{}}s:8:\\\"channels\\\";a:1:{i:0;s:8:\\\"database\\\";}s:5:\\\"tries\\\";N;s:7:\\\"timeout\\\";N;s:3:\\\"job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:19:\\\"chainCatchCallbacks\\\";N;s:5:\\\"delay\\\";N;s:11:\\\"afterCommit\\\";N;s:10:\\\"middleware\\\";a:0:{}s:7:\\\"chained\\\";a:0:{}}\"}}', 0, NULL, 1616996003, 1616996003),
(219, 'default', '{\"uuid\":\"43d28907-e424-4942-9ff5-90f8b65460ed\",\"displayName\":\"App\\\\Notifications\\\\EmployeeSalaryNotification\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"maxExceptions\":null,\"backoff\":\"\",\"timeout\":null,\"retryUntil\":null,\"data\":{\"commandName\":\"Illuminate\\\\Notifications\\\\SendQueuedNotifications\",\"command\":\"O:48:\\\"Illuminate\\\\Notifications\\\\SendQueuedNotifications\\\":15:{s:11:\\\"notifiables\\\";O:45:\\\"Illuminate\\\\Contracts\\\\Database\\\\ModelIdentifier\\\":4:{s:5:\\\"class\\\";s:15:\\\"App\\\\Models\\\\User\\\";s:2:\\\"id\\\";a:1:{i:0;i:1;}s:9:\\\"relations\\\";a:0:{}s:10:\\\"connection\\\";s:5:\\\"mysql\\\";}s:12:\\\"notification\\\";O:44:\\\"App\\\\Notifications\\\\EmployeeSalaryNotification\\\":12:{s:4:\\\"user\\\";O:45:\\\"Illuminate\\\\Contracts\\\\Database\\\\ModelIdentifier\\\":4:{s:5:\\\"class\\\";s:15:\\\"App\\\\Models\\\\User\\\";s:2:\\\"id\\\";i:1;s:9:\\\"relations\\\";a:0:{}s:10:\\\"connection\\\";s:5:\\\"mysql\\\";}s:2:\\\"id\\\";s:36:\\\"57dc64ec-b9b2-4975-b67e-ccf96ceec340\\\";s:6:\\\"locale\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:19:\\\"chainCatchCallbacks\\\";N;s:5:\\\"delay\\\";N;s:11:\\\"afterCommit\\\";N;s:10:\\\"middleware\\\";a:0:{}s:7:\\\"chained\\\";a:0:{}}s:8:\\\"channels\\\";a:1:{i:0;s:8:\\\"database\\\";}s:5:\\\"tries\\\";N;s:7:\\\"timeout\\\";N;s:3:\\\"job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:19:\\\"chainCatchCallbacks\\\";N;s:5:\\\"delay\\\";N;s:11:\\\"afterCommit\\\";N;s:10:\\\"middleware\\\";a:0:{}s:7:\\\"chained\\\";a:0:{}}\"}}', 0, NULL, 1616996003, 1616996003),
(220, 'default', '{\"uuid\":\"b47026ff-4c9e-476e-aa5d-f5a244eb92c3\",\"displayName\":\"App\\\\Notifications\\\\EmployeeSalaryNotification\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"maxExceptions\":null,\"backoff\":\"\",\"timeout\":null,\"retryUntil\":null,\"data\":{\"commandName\":\"Illuminate\\\\Notifications\\\\SendQueuedNotifications\",\"command\":\"O:48:\\\"Illuminate\\\\Notifications\\\\SendQueuedNotifications\\\":15:{s:11:\\\"notifiables\\\";O:45:\\\"Illuminate\\\\Contracts\\\\Database\\\\ModelIdentifier\\\":4:{s:5:\\\"class\\\";s:15:\\\"App\\\\Models\\\\User\\\";s:2:\\\"id\\\";a:1:{i:0;i:241;}s:9:\\\"relations\\\";a:0:{}s:10:\\\"connection\\\";s:5:\\\"mysql\\\";}s:12:\\\"notification\\\";O:44:\\\"App\\\\Notifications\\\\EmployeeSalaryNotification\\\":12:{s:4:\\\"user\\\";O:45:\\\"Illuminate\\\\Contracts\\\\Database\\\\ModelIdentifier\\\":4:{s:5:\\\"class\\\";s:15:\\\"App\\\\Models\\\\User\\\";s:2:\\\"id\\\";i:241;s:9:\\\"relations\\\";a:0:{}s:10:\\\"connection\\\";s:5:\\\"mysql\\\";}s:2:\\\"id\\\";s:36:\\\"c7300641-4f9f-4c35-baa2-e56b09148f4b\\\";s:6:\\\"locale\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:19:\\\"chainCatchCallbacks\\\";N;s:5:\\\"delay\\\";N;s:11:\\\"afterCommit\\\";N;s:10:\\\"middleware\\\";a:0:{}s:7:\\\"chained\\\";a:0:{}}s:8:\\\"channels\\\";a:1:{i:0;s:8:\\\"database\\\";}s:5:\\\"tries\\\";N;s:7:\\\"timeout\\\";N;s:3:\\\"job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:19:\\\"chainCatchCallbacks\\\";N;s:5:\\\"delay\\\";N;s:11:\\\"afterCommit\\\";N;s:10:\\\"middleware\\\";a:0:{}s:7:\\\"chained\\\";a:0:{}}\"}}', 0, NULL, 1616996003, 1616996003),
(221, 'default', '{\"uuid\":\"458f0998-fe09-4248-a11e-a9f562ff8abc\",\"displayName\":\"App\\\\Notifications\\\\EmployeeSalaryNotification\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"maxExceptions\":null,\"backoff\":\"\",\"timeout\":null,\"retryUntil\":null,\"data\":{\"commandName\":\"Illuminate\\\\Notifications\\\\SendQueuedNotifications\",\"command\":\"O:48:\\\"Illuminate\\\\Notifications\\\\SendQueuedNotifications\\\":15:{s:11:\\\"notifiables\\\";O:45:\\\"Illuminate\\\\Contracts\\\\Database\\\\ModelIdentifier\\\":4:{s:5:\\\"class\\\";s:15:\\\"App\\\\Models\\\\User\\\";s:2:\\\"id\\\";a:1:{i:0;i:1;}s:9:\\\"relations\\\";a:0:{}s:10:\\\"connection\\\";s:5:\\\"mysql\\\";}s:12:\\\"notification\\\";O:44:\\\"App\\\\Notifications\\\\EmployeeSalaryNotification\\\":12:{s:4:\\\"user\\\";O:45:\\\"Illuminate\\\\Contracts\\\\Database\\\\ModelIdentifier\\\":4:{s:5:\\\"class\\\";s:15:\\\"App\\\\Models\\\\User\\\";s:2:\\\"id\\\";i:1;s:9:\\\"relations\\\";a:0:{}s:10:\\\"connection\\\";s:5:\\\"mysql\\\";}s:2:\\\"id\\\";s:36:\\\"2a8aa2f0-eb4d-4bfa-932f-fc9a9071ded7\\\";s:6:\\\"locale\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:19:\\\"chainCatchCallbacks\\\";N;s:5:\\\"delay\\\";N;s:11:\\\"afterCommit\\\";N;s:10:\\\"middleware\\\";a:0:{}s:7:\\\"chained\\\";a:0:{}}s:8:\\\"channels\\\";a:1:{i:0;s:8:\\\"database\\\";}s:5:\\\"tries\\\";N;s:7:\\\"timeout\\\";N;s:3:\\\"job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:19:\\\"chainCatchCallbacks\\\";N;s:5:\\\"delay\\\";N;s:11:\\\"afterCommit\\\";N;s:10:\\\"middleware\\\";a:0:{}s:7:\\\"chained\\\";a:0:{}}\"}}', 0, NULL, 1616996011, 1616996011),
(222, 'default', '{\"uuid\":\"c7d3f843-3cdf-4749-bed8-ae5f52a9c27f\",\"displayName\":\"App\\\\Notifications\\\\EmployeeSalaryNotification\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"maxExceptions\":null,\"backoff\":\"\",\"timeout\":null,\"retryUntil\":null,\"data\":{\"commandName\":\"Illuminate\\\\Notifications\\\\SendQueuedNotifications\",\"command\":\"O:48:\\\"Illuminate\\\\Notifications\\\\SendQueuedNotifications\\\":15:{s:11:\\\"notifiables\\\";O:45:\\\"Illuminate\\\\Contracts\\\\Database\\\\ModelIdentifier\\\":4:{s:5:\\\"class\\\";s:15:\\\"App\\\\Models\\\\User\\\";s:2:\\\"id\\\";a:1:{i:0;i:1;}s:9:\\\"relations\\\";a:0:{}s:10:\\\"connection\\\";s:5:\\\"mysql\\\";}s:12:\\\"notification\\\";O:44:\\\"App\\\\Notifications\\\\EmployeeSalaryNotification\\\":12:{s:4:\\\"user\\\";O:45:\\\"Illuminate\\\\Contracts\\\\Database\\\\ModelIdentifier\\\":4:{s:5:\\\"class\\\";s:15:\\\"App\\\\Models\\\\User\\\";s:2:\\\"id\\\";i:1;s:9:\\\"relations\\\";a:0:{}s:10:\\\"connection\\\";s:5:\\\"mysql\\\";}s:2:\\\"id\\\";s:36:\\\"82dac65a-d2e1-45bb-b278-8080a2d0d8ea\\\";s:6:\\\"locale\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:19:\\\"chainCatchCallbacks\\\";N;s:5:\\\"delay\\\";N;s:11:\\\"afterCommit\\\";N;s:10:\\\"middleware\\\";a:0:{}s:7:\\\"chained\\\";a:0:{}}s:8:\\\"channels\\\";a:1:{i:0;s:8:\\\"database\\\";}s:5:\\\"tries\\\";N;s:7:\\\"timeout\\\";N;s:3:\\\"job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:19:\\\"chainCatchCallbacks\\\";N;s:5:\\\"delay\\\";N;s:11:\\\"afterCommit\\\";N;s:10:\\\"middleware\\\";a:0:{}s:7:\\\"chained\\\";a:0:{}}\"}}', 0, NULL, 1616996012, 1616996012),
(223, 'default', '{\"uuid\":\"79db3fdd-3690-47f8-bb55-76121ac374aa\",\"displayName\":\"App\\\\Notifications\\\\EmployeeSalaryNotification\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"maxExceptions\":null,\"backoff\":\"\",\"timeout\":null,\"retryUntil\":null,\"data\":{\"commandName\":\"Illuminate\\\\Notifications\\\\SendQueuedNotifications\",\"command\":\"O:48:\\\"Illuminate\\\\Notifications\\\\SendQueuedNotifications\\\":15:{s:11:\\\"notifiables\\\";O:45:\\\"Illuminate\\\\Contracts\\\\Database\\\\ModelIdentifier\\\":4:{s:5:\\\"class\\\";s:15:\\\"App\\\\Models\\\\User\\\";s:2:\\\"id\\\";a:1:{i:0;i:241;}s:9:\\\"relations\\\";a:0:{}s:10:\\\"connection\\\";s:5:\\\"mysql\\\";}s:12:\\\"notification\\\";O:44:\\\"App\\\\Notifications\\\\EmployeeSalaryNotification\\\":12:{s:4:\\\"user\\\";O:45:\\\"Illuminate\\\\Contracts\\\\Database\\\\ModelIdentifier\\\":4:{s:5:\\\"class\\\";s:15:\\\"App\\\\Models\\\\User\\\";s:2:\\\"id\\\";i:241;s:9:\\\"relations\\\";a:0:{}s:10:\\\"connection\\\";s:5:\\\"mysql\\\";}s:2:\\\"id\\\";s:36:\\\"57254341-209c-4e86-837e-05e7976aef95\\\";s:6:\\\"locale\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:19:\\\"chainCatchCallbacks\\\";N;s:5:\\\"delay\\\";N;s:11:\\\"afterCommit\\\";N;s:10:\\\"middleware\\\";a:0:{}s:7:\\\"chained\\\";a:0:{}}s:8:\\\"channels\\\";a:1:{i:0;s:8:\\\"database\\\";}s:5:\\\"tries\\\";N;s:7:\\\"timeout\\\";N;s:3:\\\"job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:19:\\\"chainCatchCallbacks\\\";N;s:5:\\\"delay\\\";N;s:11:\\\"afterCommit\\\";N;s:10:\\\"middleware\\\";a:0:{}s:7:\\\"chained\\\";a:0:{}}\"}}', 0, NULL, 1616996012, 1616996012),
(224, 'default', '{\"uuid\":\"d14fb89e-e3bb-447b-946f-d720d774a7ce\",\"displayName\":\"App\\\\Notifications\\\\EmployeeSalaryNotification\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"maxExceptions\":null,\"backoff\":\"\",\"timeout\":null,\"retryUntil\":null,\"data\":{\"commandName\":\"Illuminate\\\\Notifications\\\\SendQueuedNotifications\",\"command\":\"O:48:\\\"Illuminate\\\\Notifications\\\\SendQueuedNotifications\\\":15:{s:11:\\\"notifiables\\\";O:45:\\\"Illuminate\\\\Contracts\\\\Database\\\\ModelIdentifier\\\":4:{s:5:\\\"class\\\";s:15:\\\"App\\\\Models\\\\User\\\";s:2:\\\"id\\\";a:1:{i:0;i:1;}s:9:\\\"relations\\\";a:0:{}s:10:\\\"connection\\\";s:5:\\\"mysql\\\";}s:12:\\\"notification\\\";O:44:\\\"App\\\\Notifications\\\\EmployeeSalaryNotification\\\":12:{s:4:\\\"user\\\";O:45:\\\"Illuminate\\\\Contracts\\\\Database\\\\ModelIdentifier\\\":4:{s:5:\\\"class\\\";s:15:\\\"App\\\\Models\\\\User\\\";s:2:\\\"id\\\";i:1;s:9:\\\"relations\\\";a:0:{}s:10:\\\"connection\\\";s:5:\\\"mysql\\\";}s:2:\\\"id\\\";s:36:\\\"013785cb-3220-4f4a-a48e-733f9e8c6b15\\\";s:6:\\\"locale\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:19:\\\"chainCatchCallbacks\\\";N;s:5:\\\"delay\\\";N;s:11:\\\"afterCommit\\\";N;s:10:\\\"middleware\\\";a:0:{}s:7:\\\"chained\\\";a:0:{}}s:8:\\\"channels\\\";a:1:{i:0;s:8:\\\"database\\\";}s:5:\\\"tries\\\";N;s:7:\\\"timeout\\\";N;s:3:\\\"job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:19:\\\"chainCatchCallbacks\\\";N;s:5:\\\"delay\\\";N;s:11:\\\"afterCommit\\\";N;s:10:\\\"middleware\\\";a:0:{}s:7:\\\"chained\\\";a:0:{}}\"}}', 0, NULL, 1617012578, 1617012578),
(225, 'default', '{\"uuid\":\"930ea596-c2dc-47cc-b1c9-676b14693cc3\",\"displayName\":\"App\\\\Notifications\\\\EmployeeSalaryNotification\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"maxExceptions\":null,\"backoff\":\"\",\"timeout\":null,\"retryUntil\":null,\"data\":{\"commandName\":\"Illuminate\\\\Notifications\\\\SendQueuedNotifications\",\"command\":\"O:48:\\\"Illuminate\\\\Notifications\\\\SendQueuedNotifications\\\":15:{s:11:\\\"notifiables\\\";O:45:\\\"Illuminate\\\\Contracts\\\\Database\\\\ModelIdentifier\\\":4:{s:5:\\\"class\\\";s:15:\\\"App\\\\Models\\\\User\\\";s:2:\\\"id\\\";a:1:{i:0;i:1;}s:9:\\\"relations\\\";a:0:{}s:10:\\\"connection\\\";s:5:\\\"mysql\\\";}s:12:\\\"notification\\\";O:44:\\\"App\\\\Notifications\\\\EmployeeSalaryNotification\\\":12:{s:4:\\\"user\\\";O:45:\\\"Illuminate\\\\Contracts\\\\Database\\\\ModelIdentifier\\\":4:{s:5:\\\"class\\\";s:15:\\\"App\\\\Models\\\\User\\\";s:2:\\\"id\\\";i:1;s:9:\\\"relations\\\";a:0:{}s:10:\\\"connection\\\";s:5:\\\"mysql\\\";}s:2:\\\"id\\\";s:36:\\\"9307156b-a4ec-4c06-975a-0cf810b0c7fd\\\";s:6:\\\"locale\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:19:\\\"chainCatchCallbacks\\\";N;s:5:\\\"delay\\\";N;s:11:\\\"afterCommit\\\";N;s:10:\\\"middleware\\\";a:0:{}s:7:\\\"chained\\\";a:0:{}}s:8:\\\"channels\\\";a:1:{i:0;s:8:\\\"database\\\";}s:5:\\\"tries\\\";N;s:7:\\\"timeout\\\";N;s:3:\\\"job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:19:\\\"chainCatchCallbacks\\\";N;s:5:\\\"delay\\\";N;s:11:\\\"afterCommit\\\";N;s:10:\\\"middleware\\\";a:0:{}s:7:\\\"chained\\\";a:0:{}}\"}}', 0, NULL, 1617012578, 1617012578),
(226, 'default', '{\"uuid\":\"fa894797-8d4c-48dc-acb5-a4171ab28938\",\"displayName\":\"App\\\\Notifications\\\\EmployeeSalaryNotification\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"maxExceptions\":null,\"backoff\":\"\",\"timeout\":null,\"retryUntil\":null,\"data\":{\"commandName\":\"Illuminate\\\\Notifications\\\\SendQueuedNotifications\",\"command\":\"O:48:\\\"Illuminate\\\\Notifications\\\\SendQueuedNotifications\\\":15:{s:11:\\\"notifiables\\\";O:45:\\\"Illuminate\\\\Contracts\\\\Database\\\\ModelIdentifier\\\":4:{s:5:\\\"class\\\";s:15:\\\"App\\\\Models\\\\User\\\";s:2:\\\"id\\\";a:1:{i:0;i:250;}s:9:\\\"relations\\\";a:0:{}s:10:\\\"connection\\\";s:5:\\\"mysql\\\";}s:12:\\\"notification\\\";O:44:\\\"App\\\\Notifications\\\\EmployeeSalaryNotification\\\":12:{s:4:\\\"user\\\";O:45:\\\"Illuminate\\\\Contracts\\\\Database\\\\ModelIdentifier\\\":4:{s:5:\\\"class\\\";s:15:\\\"App\\\\Models\\\\User\\\";s:2:\\\"id\\\";i:250;s:9:\\\"relations\\\";a:0:{}s:10:\\\"connection\\\";s:5:\\\"mysql\\\";}s:2:\\\"id\\\";s:36:\\\"65c047e1-6534-4b52-97fb-c58795a23e01\\\";s:6:\\\"locale\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:19:\\\"chainCatchCallbacks\\\";N;s:5:\\\"delay\\\";N;s:11:\\\"afterCommit\\\";N;s:10:\\\"middleware\\\";a:0:{}s:7:\\\"chained\\\";a:0:{}}s:8:\\\"channels\\\";a:1:{i:0;s:8:\\\"database\\\";}s:5:\\\"tries\\\";N;s:7:\\\"timeout\\\";N;s:3:\\\"job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:19:\\\"chainCatchCallbacks\\\";N;s:5:\\\"delay\\\";N;s:11:\\\"afterCommit\\\";N;s:10:\\\"middleware\\\";a:0:{}s:7:\\\"chained\\\";a:0:{}}\"}}', 0, NULL, 1617012578, 1617012578);

-- --------------------------------------------------------

--
-- Table structure for table `job_shift`
--

CREATE TABLE `job_shift` (
  `id` int NOT NULL,
  `serial_no` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `job_id` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `crm_id` int NOT NULL,
  `words` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `shifted_from` int NOT NULL,
  `shifted_to` int NOT NULL,
  `shifted_date` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `delivery_date` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `payable` int NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Table structure for table `leaves`
--

CREATE TABLE `leaves` (
  `id` int UNSIGNED NOT NULL,
  `emp_id` int UNSIGNED NOT NULL,
  `leave_type` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `date_from` date NOT NULL,
  `date_to` date NOT NULL,
  `days` bigint NOT NULL,
  `reason` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `leave_type_offer` tinyint NOT NULL DEFAULT '0',
  `is_approved` tinyint(1) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `menus`
--

CREATE TABLE `menus` (
  `id` bigint UNSIGNED NOT NULL,
  `menu_title` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `parent_id` int NOT NULL DEFAULT '0',
  `sort_order` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  `slug` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `menus`
--

INSERT INTO `menus` (`id`, `menu_title`, `parent_id`, `sort_order`, `slug`, `created_at`, `updated_at`) VALUES
(1, 'Admin', 0, '0', '/admin', '2020-09-25 19:40:58', '2020-09-25 19:40:58'),
(2, 'Leave', 0, '2', '/leave', '2020-09-25 19:40:58', '2020-09-25 19:40:58'),
(3, 'Apply', 2, '2', '/apply', '2020-09-25 19:40:58', '2020-09-25 19:40:58'),
(4, 'My Leave', 2, '3', '/myleave', '2020-09-25 19:40:58', '2020-09-25 19:40:58'),
(5, 'About Team', 4, '4', '/about-team', '2020-09-25 19:40:58', '2020-09-25 19:40:58'),
(6, 'About Clients', 4, '3', '/about-clients', '2020-09-25 19:40:58', '2020-09-25 19:40:58'),
(7, 'Contact Team', 5, '3', '/contact-team', '2020-09-25 19:40:58', '2020-09-25 19:40:58'),
(8, 'Contact Clients', 6, '3', '/contact-clients', '2020-09-25 19:40:58', '2020-09-25 19:40:58'),
(9, 'Entitlements', 2, '4', '/entitlements', '2020-09-25 19:40:58', '2020-09-25 19:40:58'),
(10, 'Reports', 2, '4', '/reports', '2020-09-25 19:40:58', '2020-09-25 19:40:58'),
(11, 'Configure', 2, '4', '/configure', '2020-09-25 19:40:58', '2020-09-25 19:40:58'),
(23, 'Time', 0, '3', '/time', NULL, NULL),
(24, 'My Info', 0, '4', '/myinfo', NULL, NULL),
(25, 'Dashboard', 0, '5', '/view-dashboard', '2021-01-08 06:20:07', '2021-01-08 06:20:07'),
(27, 'PIM', 0, '7', '/buzz', '2021-01-03 10:58:46', '2021-01-03 10:58:46'),
(28, 'Leave List', 2, '5', '/leavelist', NULL, NULL),
(29, 'Assign Leave', 2, '6', '/assignleave', NULL, NULL),
(30, 'User Management', 1, '1', '/usermanagement', '2021-01-03 10:16:26', '2021-01-03 10:16:26'),
(31, 'Job', 1, '2', '/job', NULL, NULL),
(32, 'Organization', 1, '3', '/organization', NULL, NULL),
(33, 'Qualification', 1, '4', '/qualification', NULL, NULL),
(34, 'Nationalities', 1, '5', '/nationality', '2021-01-06 13:56:41', '2021-01-06 13:56:41'),
(37, 'Configuration', 27, '8', '/configuration', '2021-01-08 04:36:30', '2021-01-08 04:36:30'),
(38, 'Employee List', 27, '9', '/employeelist', NULL, NULL),
(39, 'Add Employee', 27, '10', '/addemployee', NULL, NULL),
(40, 'Reports', 27, '11', '/reports', NULL, NULL),
(41, 'Users', 30, '1', '/users', '2021-01-04 15:58:28', '2021-01-04 15:58:28'),
(42, 'User Roles', 30, '2', '/view-role', '2021-01-06 14:21:56', '2021-01-06 14:21:56'),
(43, 'User Responsibilities', 30, '3', '/view-sub-role', '2021-01-06 15:21:14', '2021-01-06 15:21:14'),
(44, 'General Information of the Organization', 32, '1', '/organazation-information', NULL, NULL),
(45, 'Companies', 32, '2', '/companies', '2021-01-08 05:24:49', '2021-01-08 05:24:49'),
(46, 'Locations', 45, '1', '/locations', '2021-01-08 05:25:31', '2021-01-08 05:25:31'),
(47, 'Departments', 46, '1', '/departments', '2021-01-08 05:25:47', '2021-01-08 05:25:47'),
(48, 'Disciplinary Actions ', 1, '6', '/configurations', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int UNSIGNED NOT NULL,
  `migration` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2019_08_19_000000_create_failed_jobs_table', 1),
(3, '2021_01_02_082936_create_menus_table', 1),
(4, '2014_10_12_100000_create_password_resets_table', 2),
(5, '2021_03_04_162847_create_notifications_table', 2),
(6, '2021_03_12_095604_create_jobs_table', 3),
(7, '2021_03_22_091635_create_announcements_table', 4);

-- --------------------------------------------------------

--
-- Table structure for table `notifications`
--

CREATE TABLE `notifications` (
  `id` char(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `notifiable_type` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `notifiable_id` bigint UNSIGNED NOT NULL,
  `data` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `read_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `projects`
--

CREATE TABLE `projects` (
  `id` int UNSIGNED NOT NULL,
  `emp_id` int NOT NULL,
  `project_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `company_id` int NOT NULL,
  `location_id` int NOT NULL,
  `planned_start_date` date DEFAULT NULL,
  `planned_end_date` date DEFAULT NULL,
  `actual_start_date` date DEFAULT NULL,
  `actual_end_date` date DEFAULT NULL,
  `project_description` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `salary_details`
--

CREATE TABLE `salary_details` (
  `id` int NOT NULL,
  `emp_id` int NOT NULL,
  `salary_issuer_id` int NOT NULL,
  `paid_amount` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `payment_date` timestamp NOT NULL,
  `payment_bank` int DEFAULT NULL,
  `salary_for_month` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `salary_status` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '0',
  `remarks` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `due` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Table structure for table `team_handover`
--

CREATE TABLE `team_handover` (
  `id` int NOT NULL,
  `emp_id` int NOT NULL,
  `handover_emp_id` int NOT NULL,
  `handover_reason` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `handover_from_date` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `handover_to_date` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint UNSIGNED NOT NULL,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `emp_id` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `role` int DEFAULT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `emp_id`, `role`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Superadmin', 'superadmin@gmail.com', '1', 1, NULL, '$2y$10$adGGfDGsI6ZmctEg8SAXhOwFshf/1LU5N/t94fj.xniNAty4wCkny', 'dkiXtCcWoyE8A6Jmh9L36eMEgBbWrxtXK5MknELB2EEXLmnUJ0PkMKs5QMa1', NULL, NULL, NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `announcements`
--
ALTER TABLE `announcements`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `azhrms_banks`
--
ALTER TABLE `azhrms_banks`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `azhrms_company_assets`
--
ALTER TABLE `azhrms_company_assets`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `azhrms_company_country`
--
ALTER TABLE `azhrms_company_country`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `azhrms_company_district`
--
ALTER TABLE `azhrms_company_district`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `azhrms_company_gen_info`
--
ALTER TABLE `azhrms_company_gen_info`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `azhrms_company_location`
--
ALTER TABLE `azhrms_company_location`
  ADD PRIMARY KEY (`id`),
  ADD KEY `operational_company_id` (`id`),
  ADD KEY `azhrms_company_ibfk_1` (`operational_company_id`);

--
-- Indexes for table `azhrms_company_location_department`
--
ALTER TABLE `azhrms_company_location_department`
  ADD PRIMARY KEY (`id`),
  ADD KEY `operational_company_location_id` (`operational_company_location_id`);

--
-- Indexes for table `azhrms_company_state`
--
ALTER TABLE `azhrms_company_state`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `azhrms_complain`
--
ALTER TABLE `azhrms_complain`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `azhrms_corporateactiontypemasters`
--
ALTER TABLE `azhrms_corporateactiontypemasters`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `azhrms_crm`
--
ALTER TABLE `azhrms_crm`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `azhrms_daily_report`
--
ALTER TABLE `azhrms_daily_report`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `azhrms_district`
--
ALTER TABLE `azhrms_district`
  ADD PRIMARY KEY (`district_code`);

--
-- Indexes for table `azhrms_education`
--
ALTER TABLE `azhrms_education`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `azhrms_employee`
--
ALTER TABLE `azhrms_employee`
  ADD PRIMARY KEY (`id`),
  ADD KEY `operational_company_loc_dept_id` (`operational_company_loc_dept_id`),
  ADD KEY `operational_company_location_id` (`operational_company_location_id`),
  ADD KEY `operational_company_id` (`operational_company_id`),
  ADD KEY `emp_religion_id` (`emp_religion_id`),
  ADD KEY `fk_skill_id` (`emp_skill_id`),
  ADD KEY `fk_education_id` (`emp_education_id`);

--
-- Indexes for table `azhrms_employee_assets`
--
ALTER TABLE `azhrms_employee_assets`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `azhrms_employee_attandance`
--
ALTER TABLE `azhrms_employee_attandance`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `azhrms_employee_bank_details`
--
ALTER TABLE `azhrms_employee_bank_details`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `azhrms_employee_edu_details`
--
ALTER TABLE `azhrms_employee_edu_details`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `azhrms_employee_feedback`
--
ALTER TABLE `azhrms_employee_feedback`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `azhrms_employee_language`
--
ALTER TABLE `azhrms_employee_language`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `azhrms_employee_promotion`
--
ALTER TABLE `azhrms_employee_promotion`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `azhrms_employee_salary`
--
ALTER TABLE `azhrms_employee_salary`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `azhrms_employee_skill_details`
--
ALTER TABLE `azhrms_employee_skill_details`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `azhrms_employee_type`
--
ALTER TABLE `azhrms_employee_type`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `azhrms_employee_warning`
--
ALTER TABLE `azhrms_employee_warning`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `azhrms_employee_work_shift`
--
ALTER TABLE `azhrms_employee_work_shift`
  ADD PRIMARY KEY (`work_shift_id`,`emp_id`),
  ADD KEY `emp_number` (`emp_id`);

--
-- Indexes for table `azhrms_emp_attachment`
--
ALTER TABLE `azhrms_emp_attachment`
  ADD PRIMARY KEY (`emp_number`,`eattach_id`),
  ADD KEY `screen` (`screen`);

--
-- Indexes for table `azhrms_emp_basicsalary`
--
ALTER TABLE `azhrms_emp_basicsalary`
  ADD PRIMARY KEY (`id`),
  ADD KEY `sal_grd_code` (`sal_grd_code`),
  ADD KEY `currency_id` (`currency_id`),
  ADD KEY `emp_number` (`emp_number`),
  ADD KEY `payperiod_code` (`payperiod_code`);

--
-- Indexes for table `azhrms_emp_children`
--
ALTER TABLE `azhrms_emp_children`
  ADD PRIMARY KEY (`emp_number`,`ec_seqno`);

--
-- Indexes for table `azhrms_emp_contract_extend`
--
ALTER TABLE `azhrms_emp_contract_extend`
  ADD PRIMARY KEY (`emp_number`,`econ_extend_id`);

--
-- Indexes for table `azhrms_emp_dependents`
--
ALTER TABLE `azhrms_emp_dependents`
  ADD PRIMARY KEY (`emp_number`,`ed_seqno`);

--
-- Indexes for table `azhrms_exam_score`
--
ALTER TABLE `azhrms_exam_score`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `azhrms_holiday`
--
ALTER TABLE `azhrms_holiday`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_azhrms_holiday_azhrms_operational_company_id` (`operational_company_id`);

--
-- Indexes for table `azhrms_jobcategory`
--
ALTER TABLE `azhrms_jobcategory`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `azhrms_jobtype`
--
ALTER TABLE `azhrms_jobtype`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `azhrms_leave_entitlement`
--
ALTER TABLE `azhrms_leave_entitlement`
  ADD PRIMARY KEY (`id`),
  ADD KEY `leave_type_id` (`leave_type_id`),
  ADD KEY `emp_number` (`emp_number`),
  ADD KEY `created_by_id` (`created_by_id`);

--
-- Indexes for table `azhrms_leave_period_history`
--
ALTER TABLE `azhrms_leave_period_history`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `azhrms_leave_type`
--
ALTER TABLE `azhrms_leave_type`
  ADD PRIMARY KEY (`id`),
  ADD KEY `operational_company_id` (`operational_company_id`),
  ADD KEY `operational_company_location_id` (`operational_company_location_id`);

--
-- Indexes for table `azhrms_nationalities`
--
ALTER TABLE `azhrms_nationalities`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `azhrms_religion`
--
ALTER TABLE `azhrms_religion`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `azhrms_skill`
--
ALTER TABLE `azhrms_skill`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `azhrms_subject`
--
ALTER TABLE `azhrms_subject`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `azhrms_time_tracker_review`
--
ALTER TABLE `azhrms_time_tracker_review`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `azhrms_training`
--
ALTER TABLE `azhrms_training`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `azhrms_user_role`
--
ALTER TABLE `azhrms_user_role`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `user_role_name` (`name`);

--
-- Indexes for table `azhrms_user_role_categories`
--
ALTER TABLE `azhrms_user_role_categories`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `user_resp_name` (`respname`);

--
-- Indexes for table `azhrms_work_shift`
--
ALTER TABLE `azhrms_work_shift`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `azhrms_work_week`
--
ALTER TABLE `azhrms_work_week`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_azhrms_workweek_azhrms_operational_company_id` (`operational_company_id`),
  ADD KEY `fk_azhrms_workweek_azhrms_operational_company_location_id` (`operational_company_location_id`),
  ADD KEY `fk_azhrms_workweek_azhrms_operational_company_location_dept_id` (`operational_company_loc_dept_id`);

--
-- Indexes for table `employee_language`
--
ALTER TABLE `employee_language`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `feedback_category`
--
ALTER TABLE `feedback_category`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `imgtest`
--
ALTER TABLE `imgtest`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `jobs`
--
ALTER TABLE `jobs`
  ADD PRIMARY KEY (`id`),
  ADD KEY `jobs_queue_index` (`queue`);

--
-- Indexes for table `job_shift`
--
ALTER TABLE `job_shift`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `leaves`
--
ALTER TABLE `leaves`
  ADD PRIMARY KEY (`id`),
  ADD KEY `leaves_employee_id_foreign` (`emp_id`);

--
-- Indexes for table `menus`
--
ALTER TABLE `menus`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `notifications`
--
ALTER TABLE `notifications`
  ADD PRIMARY KEY (`id`),
  ADD KEY `notifications_notifiable_type_notifiable_id_index` (`notifiable_type`,`notifiable_id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `projects`
--
ALTER TABLE `projects`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `salary_details`
--
ALTER TABLE `salary_details`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `team_handover`
--
ALTER TABLE `team_handover`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `announcements`
--
ALTER TABLE `announcements`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `azhrms_banks`
--
ALTER TABLE `azhrms_banks`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `azhrms_company_assets`
--
ALTER TABLE `azhrms_company_assets`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `azhrms_company_country`
--
ALTER TABLE `azhrms_company_country`
  MODIFY `id` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `azhrms_company_district`
--
ALTER TABLE `azhrms_company_district`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=98;

--
-- AUTO_INCREMENT for table `azhrms_company_gen_info`
--
ALTER TABLE `azhrms_company_gen_info`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `azhrms_company_location`
--
ALTER TABLE `azhrms_company_location`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `azhrms_company_location_department`
--
ALTER TABLE `azhrms_company_location_department`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `azhrms_company_state`
--
ALTER TABLE `azhrms_company_state`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;

--
-- AUTO_INCREMENT for table `azhrms_complain`
--
ALTER TABLE `azhrms_complain`
  MODIFY `id` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `azhrms_corporateactiontypemasters`
--
ALTER TABLE `azhrms_corporateactiontypemasters`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `azhrms_crm`
--
ALTER TABLE `azhrms_crm`
  MODIFY `id` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `azhrms_daily_report`
--
ALTER TABLE `azhrms_daily_report`
  MODIFY `id` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `azhrms_education`
--
ALTER TABLE `azhrms_education`
  MODIFY `id` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `azhrms_employee`
--
ALTER TABLE `azhrms_employee`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `azhrms_employee_assets`
--
ALTER TABLE `azhrms_employee_assets`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `azhrms_employee_attandance`
--
ALTER TABLE `azhrms_employee_attandance`
  MODIFY `id` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `azhrms_employee_bank_details`
--
ALTER TABLE `azhrms_employee_bank_details`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `azhrms_employee_edu_details`
--
ALTER TABLE `azhrms_employee_edu_details`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `azhrms_employee_feedback`
--
ALTER TABLE `azhrms_employee_feedback`
  MODIFY `id` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `azhrms_employee_language`
--
ALTER TABLE `azhrms_employee_language`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `azhrms_employee_promotion`
--
ALTER TABLE `azhrms_employee_promotion`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `azhrms_employee_salary`
--
ALTER TABLE `azhrms_employee_salary`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `azhrms_employee_skill_details`
--
ALTER TABLE `azhrms_employee_skill_details`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `azhrms_employee_type`
--
ALTER TABLE `azhrms_employee_type`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `azhrms_employee_warning`
--
ALTER TABLE `azhrms_employee_warning`
  MODIFY `id` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `azhrms_employee_work_shift`
--
ALTER TABLE `azhrms_employee_work_shift`
  MODIFY `work_shift_id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `azhrms_emp_basicsalary`
--
ALTER TABLE `azhrms_emp_basicsalary`
  MODIFY `id` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `azhrms_exam_score`
--
ALTER TABLE `azhrms_exam_score`
  MODIFY `id` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `azhrms_holiday`
--
ALTER TABLE `azhrms_holiday`
  MODIFY `id` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `azhrms_jobcategory`
--
ALTER TABLE `azhrms_jobcategory`
  MODIFY `id` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `azhrms_jobtype`
--
ALTER TABLE `azhrms_jobtype`
  MODIFY `id` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `azhrms_leave_entitlement`
--
ALTER TABLE `azhrms_leave_entitlement`
  MODIFY `id` int UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `azhrms_leave_period_history`
--
ALTER TABLE `azhrms_leave_period_history`
  MODIFY `id` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `azhrms_leave_type`
--
ALTER TABLE `azhrms_leave_type`
  MODIFY `id` int UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `azhrms_nationalities`
--
ALTER TABLE `azhrms_nationalities`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `azhrms_religion`
--
ALTER TABLE `azhrms_religion`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=194;

--
-- AUTO_INCREMENT for table `azhrms_skill`
--
ALTER TABLE `azhrms_skill`
  MODIFY `id` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `azhrms_subject`
--
ALTER TABLE `azhrms_subject`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `azhrms_time_tracker_review`
--
ALTER TABLE `azhrms_time_tracker_review`
  MODIFY `id` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `azhrms_training`
--
ALTER TABLE `azhrms_training`
  MODIFY `id` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `azhrms_user_role`
--
ALTER TABLE `azhrms_user_role`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `azhrms_user_role_categories`
--
ALTER TABLE `azhrms_user_role_categories`
  MODIFY `id` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `azhrms_work_shift`
--
ALTER TABLE `azhrms_work_shift`
  MODIFY `id` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `azhrms_work_week`
--
ALTER TABLE `azhrms_work_week`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `employee_language`
--
ALTER TABLE `employee_language`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `feedback_category`
--
ALTER TABLE `feedback_category`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `imgtest`
--
ALTER TABLE `imgtest`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `jobs`
--
ALTER TABLE `jobs`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=227;

--
-- AUTO_INCREMENT for table `job_shift`
--
ALTER TABLE `job_shift`
  MODIFY `id` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `leaves`
--
ALTER TABLE `leaves`
  MODIFY `id` int UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `menus`
--
ALTER TABLE `menus`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=49;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `projects`
--
ALTER TABLE `projects`
  MODIFY `id` int UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `salary_details`
--
ALTER TABLE `salary_details`
  MODIFY `id` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `team_handover`
--
ALTER TABLE `team_handover`
  MODIFY `id` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `azhrms_company_location`
--
ALTER TABLE `azhrms_company_location`
  ADD CONSTRAINT `azhrms_company_ibfk_1` FOREIGN KEY (`operational_company_id`) REFERENCES `azhrms_company_gen_info` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `azhrms_company_location_department`
--
ALTER TABLE `azhrms_company_location_department`
  ADD CONSTRAINT `azhrms_company_location_ibfk_1` FOREIGN KEY (`operational_company_location_id`) REFERENCES `azhrms_company_location` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `azhrms_employee`
--
ALTER TABLE `azhrms_employee`
  ADD CONSTRAINT `azhrms_employee_ibfk_1` FOREIGN KEY (`operational_company_loc_dept_id`) REFERENCES `azhrms_company_location_department` (`id`) ON DELETE SET NULL,
  ADD CONSTRAINT `azhrms_employee_ibfk_2` FOREIGN KEY (`operational_company_location_id`) REFERENCES `azhrms_company_location` (`id`) ON DELETE SET NULL,
  ADD CONSTRAINT `azhrms_employee_ibfk_3` FOREIGN KEY (`operational_company_id`) REFERENCES `azhrms_company_gen_info` (`id`) ON DELETE SET NULL,
  ADD CONSTRAINT `azhrms_employee_ibfk_4` FOREIGN KEY (`emp_religion_id`) REFERENCES `azhrms_religion` (`id`) ON DELETE SET NULL,
  ADD CONSTRAINT `fk_education_id` FOREIGN KEY (`emp_education_id`) REFERENCES `azhrms_education` (`id`),
  ADD CONSTRAINT `fk_skill_id` FOREIGN KEY (`emp_skill_id`) REFERENCES `azhrms_skill` (`id`);

--
-- Constraints for table `azhrms_employee_work_shift`
--
ALTER TABLE `azhrms_employee_work_shift`
  ADD CONSTRAINT `azhrms_employee_work_shift_ibfk_1` FOREIGN KEY (`work_shift_id`) REFERENCES `azhrms_work_shift` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `azhrms_employee_work_shift_ibfk_2` FOREIGN KEY (`emp_id`) REFERENCES `azhrms_employee` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `azhrms_holiday`
--
ALTER TABLE `azhrms_holiday`
  ADD CONSTRAINT `fk_azhrms_holiday_azhrms_operational_company_id` FOREIGN KEY (`operational_company_id`) REFERENCES `azhrms_company_gen_info` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `azhrms_leave_entitlement`
--
ALTER TABLE `azhrms_leave_entitlement`
  ADD CONSTRAINT `azhrms_leave_entitlement_ibfk_1` FOREIGN KEY (`leave_type_id`) REFERENCES `azhrms_leave_type` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `azhrms_leave_entitlement_ibfk_2` FOREIGN KEY (`emp_number`) REFERENCES `azhrms_employee` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `azhrms_leave_entitlement_ibfk_3` FOREIGN KEY (`created_by_id`) REFERENCES `users` (`id`) ON DELETE SET NULL;

--
-- Constraints for table `azhrms_leave_type`
--
ALTER TABLE `azhrms_leave_type`
  ADD CONSTRAINT `azhrms_leave_type_ibfk_1` FOREIGN KEY (`operational_company_id`) REFERENCES `azhrms_company_gen_info` (`id`) ON DELETE SET NULL,
  ADD CONSTRAINT `azhrms_leave_type_ibfk_2` FOREIGN KEY (`operational_company_location_id`) REFERENCES `azhrms_company_location` (`id`) ON DELETE SET NULL;

--
-- Constraints for table `azhrms_work_week`
--
ALTER TABLE `azhrms_work_week`
  ADD CONSTRAINT `fk_azhrms_work_week_azhrms_operational_company_id` FOREIGN KEY (`operational_company_id`) REFERENCES `azhrms_company_gen_info` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_azhrms_work_week_azhrms_operational_company_location_dept_id` FOREIGN KEY (`operational_company_loc_dept_id`) REFERENCES `azhrms_company_location_department` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_azhrms_work_week_azhrms_operational_company_location_id` FOREIGN KEY (`operational_company_location_id`) REFERENCES `azhrms_company_location` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
